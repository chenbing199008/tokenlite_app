/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 0);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/admin.chart.js":
/*!*************************************!*\
  !*** ./resources/js/admin.chart.js ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/*! TokenLite v1.1.5 | Copyright by Softnio. */
!function (t) {
  "use strict";

  if (t("#tknSale").length > 0) {
    var e = document.getElementById("tknSale").getContext("2d"),
        o = new Chart(e, {
      type: "line",
      data: {
        labels: tnx_labels,
        datasets: [{
          label: "",
          tension: 0.4,
          backgroundColor: "transparent",
          borderColor: theme_color.base,
          pointBorderColor: theme_color.base,
          pointBackgroundColor: "#fff",
          pointBorderWidth: 2,
          pointHoverRadius: 6,
          pointHoverBackgroundColor: "#fff",
          pointHoverBorderColor: theme_color.base,
          pointHoverBorderWidth: 2,
          pointRadius: 6,
          pointHitRadius: 6,
          data: tnx_data
        }]
      },
      options: {
        legend: {
          display: !1
        },
        maintainAspectRatio: !1,
        tooltips: {
          callbacks: {
            title: function title(t, e) {
              return "Date : " + e.labels[t[0].index];
            },
            label: function label(t, e) {
              return e.datasets[0].data[t.index] + " Tokens";
            }
          },
          backgroundColor: "#f2f4f7",
          titleFontSize: 13,
          titleFontColor: theme_color.heading,
          titleMarginBottom: 10,
          bodyFontColor: theme_color.text,
          bodyFontSize: 14,
          bodySpacing: 4,
          yPadding: 15,
          xPadding: 15,
          footerMarginTop: 5,
          displayColors: !1
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: !0,
              fontSize: 12,
              fontColor: theme_color.text
            },
            gridLines: {
              color: "#e9edf3",
              tickMarkLength: 0,
              zeroLineColor: "#e9edf3"
            }
          }],
          xAxes: [{
            ticks: {
              fontSize: 12,
              fontColor: theme_color.text,
              source: "auto"
            },
            gridLines: {
              color: "transparent",
              tickMarkLength: 20,
              zeroLineColor: "#e9edf3"
            }
          }]
        }
      }
    });
    t(".token-sale-graph li a").on("click", function (e) {
      e.preventDefault();
      var a = t(this),
          r = t(this).attr("href");
      t.get(r).done(function (t) {
        o.data.labels = Object.values(t.chart.days_alt), o.data.datasets[0].data = Object.values(t.chart.data_alt), o.update(), a.parents(".token-sale-graph").find("a.toggle-tigger").text(a.text()), a.closest(".toggle-class").toggleClass("active");
      });
    });
  }

  if (t("#BountytknSale").length > 0) {
    var e = document.getElementById("BountytknSale").getContext("2d"),
        o = new Chart(e, {
      type: "line",
      data: {
        labels: tnx_labels,
        datasets: [{
          label: "",
          tension: 0.4,
          backgroundColor: "transparent",
          borderColor: theme_color.base,
          pointBorderColor: theme_color.base,
          pointBackgroundColor: "#fff",
          pointBorderWidth: 2,
          pointHoverRadius: 6,
          pointHoverBackgroundColor: "#fff",
          pointHoverBorderColor: theme_color.base,
          pointHoverBorderWidth: 2,
          pointRadius: 6,
          pointHitRadius: 6,
          data: tnx_bounty_data
        }]
      },
      options: {
        legend: {
          display: !1
        },
        maintainAspectRatio: !1,
        tooltips: {
          callbacks: {
            title: function title(t, e) {
              return "Date : " + e.labels[t[0].index];
            },
            label: function label(t, e) {
              return e.datasets[0].data[t.index] + " Tokens";
            }
          },
          backgroundColor: "#f2f4f7",
          titleFontSize: 13,
          titleFontColor: theme_color.heading,
          titleMarginBottom: 10,
          bodyFontColor: theme_color.text,
          bodyFontSize: 14,
          bodySpacing: 4,
          yPadding: 15,
          xPadding: 15,
          footerMarginTop: 5,
          displayColors: !1
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: !0,
              fontSize: 12,
              fontColor: theme_color.text
            },
            gridLines: {
              color: "#e9edf3",
              tickMarkLength: 0,
              zeroLineColor: "#e9edf3"
            }
          }],
          xAxes: [{
            ticks: {
              fontSize: 12,
              fontColor: theme_color.text,
              source: "auto"
            },
            gridLines: {
              color: "transparent",
              tickMarkLength: 20,
              zeroLineColor: "#e9edf3"
            }
          }]
        }
      }
    });
    t(".token-sale-graph li a").on("click", function (e) {
      e.preventDefault();
      var a = t(this),
          r = t(this).attr("href");
      t.get(r).done(function (t) {
        o.data.labels = Object.values(t.chart.days_alt), o.data.datasets[0].data = Object.values(t.chart.data_alt), o.update(), a.parents(".token-sale-graph").find("a.toggle-tigger").text(a.text()), a.closest(".toggle-class").toggleClass("active");
      });
    });
  }

  if (t("#regStatistics").length > 0) {
    var a = document.getElementById("regStatistics").getContext("2d");
    var gradient = a.createLinearGradient(0, 0, 0, 100);
    gradient.addColorStop(0, "#21BBEE");
    gradient.addColorStop(0.5458, "#07BCC3");
    gradient.addColorStop(1, "#50BD8C");
    var r = new Chart(a, {
      type: "bar",
      data: {
        labels: user_labels,
        datasets: [{
          label: "",
          lineTension: 0,
          backgroundColor: gradient,
          hoverBackgroundColor: gradient,
          borderColor: theme_color.base,
          barThickness: 0.4,
          data: user_data
        }]
      },
      options: {
        legend: {
          display: !1
        },
        maintainAspectRatio: !1,
        tooltips: {
          callbacks: {
            title: function title(t, e) {
              return !1;
            },
            label: function label(t, e) {
              return e.datasets[0].data[t.index] + " ";
            }
          },
          backgroundColor: "#f2f4f7",
          bodyFontColor: theme_color.base,
          bodyFontSize: 14,
          bodySpacing: 5,
          yPadding: 3,
          xPadding: 10,
          footerMarginTop: 10,
          displayColors: !1
        },
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: !0,
              fontSize: 10,
              fontColor: theme_color.text
            },
            gridLines: {
              color: "transparent",
              tickMarkLength: 0,
              zeroLineColor: "transparent"
            }
          }],
          xAxes: [{
            ticks: {
              fontSize: 9,
              fontColor: theme_color.text,
              source: "auto"
            },
            gridLines: {
              color: "transparent",
              tickMarkLength: 7,
              zeroLineColor: "transparent"
            }
          }]
        }
      }
    });
    t(".reg-statistic-graph li a").on("click", function (e) {
      e.preventDefault();
      var o = t(this),
          a = t(this).attr("href");
      t.get(a).done(function (t) {
        r.data.labels = Object.values(t.chart.days_alt), r.data.datasets[0].data = Object.values(t.chart.data_alt), r.update(), o.parents(".reg-statistic-graph").find("a.toggle-tigger").text(o.text()), o.closest(".toggle-class").toggleClass("active");
      });
    });
  }

  window.pieColors = {
    pieColor1: "#33BD5B",
    pieColor2: "#BDA033"
  };

  if (t("#phaseStatus").length > 0) {
    var n = document.getElementById("phaseStatus").getContext("2d");
    new Chart(n, {
      type: "doughnut",
      data: {
        labels: ["Total Sold", "Unsold Tokens"],
        datasets: [{
          lineTension: 0,
          backgroundColor: [window.pieColors.pieColor1, window.pieColors.pieColor2],
          borderColor: "#fff",
          borderWidth: 2,
          hoverBorderColor: "#fff",
          data: phase_data
        }]
      },
      options: {
        legend: {
          display: !1,
          labels: {
            boxWidth: 10,
            fontColor: "#000"
          }
        },
        rotation: -1.6,
        cutoutPercentage: 80,
        maintainAspectRatio: !1,
        tooltips: {
          callbacks: {
            title: function title(t, e) {
              return e.labels[t[0].index];
            },
            label: function label(t, e) {
              return e.datasets[0].data[t.index] + " ";
            }
          },
          backgroundColor: "#f2f4f7",
          titleFontSize: 13,
          titleFontColor: theme_color.heading,
          titleMarginBottom: 10,
          bodyFontColor: theme_color.text,
          bodyFontSize: 14,
          bodySpacing: 4,
          yPadding: 15,
          xPadding: 15,
          footerMarginTop: 5,
          displayColors: !1
        }
      }
    });
  }

  if (t("#BountyPhaseStatus").length > 0) {
    var n = document.getElementById("BountyPhaseStatus").getContext("2d");
    new Chart(n, {
      type: "doughnut",
      data: {
        labels: ["Distributed", "Undistributed"],
        datasets: [{
          lineTension: 0,
          backgroundColor: [window.pieColors.pieColor1, window.pieColors.pieColor2],
          borderColor: "#fff",
          borderWidth: 2,
          hoverBorderColor: "#fff",
          data: phase_bounty_data
        }]
      },
      options: {
        legend: {
          display: !1,
          labels: {
            boxWidth: 10,
            fontColor: "#000"
          }
        },
        rotation: -1.6,
        cutoutPercentage: 80,
        maintainAspectRatio: !1,
        tooltips: {
          callbacks: {
            title: function title(t, e) {
              return e.labels[t[0].index];
            },
            label: function label(t, e) {
              return e.datasets[0].data[t.index] + " ";
            }
          },
          backgroundColor: "#f2f4f7",
          titleFontSize: 13,
          titleFontColor: theme_color.heading,
          titleMarginBottom: 10,
          bodyFontColor: theme_color.text,
          bodyFontSize: 14,
          bodySpacing: 4,
          yPadding: 15,
          xPadding: 15,
          footerMarginTop: 5,
          displayColors: !1
        }
      }
    });
  }
}(jQuery);

/***/ }),

/***/ "./resources/sass/bounty.scss":
/*!************************************!*\
  !*** ./resources/sass/bounty.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./resources/sass/custom.scss":
/*!************************************!*\
  !*** ./resources/sass/custom.scss ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ "./resources/sass/style.scss":
/*!***********************************!*\
  !*** ./resources/sass/style.scss ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 0:
/*!*********************************************************************************************************************************!*\
  !*** multi ./resources/js/admin.chart.js ./resources/sass/style.scss ./resources/sass/bounty.scss ./resources/sass/custom.scss ***!
  \*********************************************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! /Volumes/Work/Laravel/Decracy-Contribution/tokenlite_app/resources/js/admin.chart.js */"./resources/js/admin.chart.js");
__webpack_require__(/*! /Volumes/Work/Laravel/Decracy-Contribution/tokenlite_app/resources/sass/style.scss */"./resources/sass/style.scss");
__webpack_require__(/*! /Volumes/Work/Laravel/Decracy-Contribution/tokenlite_app/resources/sass/bounty.scss */"./resources/sass/bounty.scss");
module.exports = __webpack_require__(/*! /Volumes/Work/Laravel/Decracy-Contribution/tokenlite_app/resources/sass/custom.scss */"./resources/sass/custom.scss");


/***/ })

/******/ });