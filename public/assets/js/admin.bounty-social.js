/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./resources/js/admin.bounty-social.js":
/*!*********************************************!*\
  !*** ./resources/js/admin.bounty-social.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$(function () {
  $("#updateNamebtn").hide();
  $("#cancelNamebtn").hide();
  $(".socialName").prop("readonly", true);
  $("#updateDCAbtn").hide();
  $("#cancelDCAbtn").hide();
  $("#dca").prop("readonly", true);
  var status = $("#socialStatus").val();

  if (status == 0) {
    $(".switch :checkbox").prop('checked', false);
  }

  if (status == 1) {
    $(".switch :checkbox").prop('checked', true);
  }

  $(".switch :checkbox").change(function () {
    var socialId = $("#socialId").val();

    if (this.checked) {
      $("#socialStatus").val(1);
    } else {
      $("#socialStatus").val(0);
    }
  });
  $(".enableEdit_btn").click(function () {
    $(".editcard").addClass("d-none");
    var index = $(".enableEdit_btn").index(this);
    $(".editcard:eq(" + index + ")").removeClass("d-none");
  });
  $(".updateRequirement_btn").click(function () {
    var index = $(".updateRequirement_btn").index(this);
    var newRequirement = $(".updated_requirement:eq(" + index + ")").val();
    $(".requirement:eq(" + index + ")").html(newRequirement);
    $(".editcard:eq(" + index + ")").addClass("d-none");
  });
  $(".cancelRequirement_btn").click(function () {
    var index = $(".cancelRequirement_btn").index(this);
    $(".editcard:eq(" + index + ")").addClass("d-none");
  });
  $(".delete_btn").click(function () {
    var index = $(".delete_btn").index(this);
    $("#confirm-modal").modal();
    $("#item_id").val(index);
  });
  $(".yes_btn").click(function () {
    var index = $("#item_id").val();
    $("#requirement_list").children().eq(index).remove();
    $("#confirm-modal").modal('toggle');
  });
  $(".update_rule_btn").click(function () {
    var index = $("#item_id").val();
    var rule = $("#rule").val();
    $("#follow_rule").html(rule);
    $("#follow_rule_count").val(rule);
  });
  $("#add_requirement_btn").click(function () {
    var new_req = $("#new_requirement").val();
    var html = '<li class="list-group-item mt-3">' + '<span class="requirement">' + new_req + '</span>' + '<div class="float-right">' + '<a class="text-success enableEdit_btn" style="cursor: pointer"><span>Edit</span></a>' + '<a class="text-danger ml-1" data-toggle="modal" data-target="#confirm-modal" data-title="" style="cursor: pointer">' + '<span>Delete</span>' + '</a>' + '</div>' + '</li>' + '<div class="card mt-5 d-none myCard editcard">' + '<div class="card-header">' + 'Edit' + '</div>' + '<div class="card-body">' + '<input type="text" class="form-control col updated_requirement" name="updated_requirement[]" value="' + new_req + '" />' + '<div class="d-flex justify-content-end mt-3">' + '<a class="btn btn-primary updateRequirement_btn"><span>OK</span></a>' + '<a class="btn btn-danger ml-1 cancelRequirement_btn"><span>Cancel</span></a>' + '</div>' + '</div>' + '</div>';
    $("#requirement_list").append(html);
    $("#new_requirement").val("");
  });

  function editName() {
    $("#editNamebtn").hide();
    $("#updateNamebtn").show();
    $("#cancelNamebtn").show();
    $(".socialName").prop("readonly", false);
  }

  function updateName() {
    $("#editNamebtn").show();
    $("#updateNamebtn").hide();
    $("#cancelNamebtn").hide();
    $(".socialName").prop("readonly", true);
  }

  function cancelName(name) {
    $("#socialName").val(name);
    $("#editNamebtn").show();
    $("#updateNamebtn").hide();
    $("#cancelNamebtn").hide();
    $(".socialName").prop("readonly", true);
  }

  function editDCA() {
    $("#editDCAbtn").hide();
    $("#updateDCAbtn").show();
    $("#cancelDCAbtn").show();
    $("#dca").prop("readonly", false);
  }

  function updateDCA() {
    $("#editDCAbtn").show();
    $("#updateDCAbtn").hide();
    $("#cancelDCAbtn").hide();
    $("#dca").prop("readonly", true);
  }

  function cancelDCA(dca) {
    $("#dca").val(dca);
    $("#editDCAbtn").show();
    $("#updateDCAbtn").hide();
    $("#cancelDCAbtn").hide();
    $("#dca").prop("readonly", true);
  }

  function cancel_requirement(id) {
    var req = $("#requirement" + id).html();
    $("#updated_requirement" + id).val(req);
    $("#editcard" + id).toggleClass("d-none");
  }

  function clearImage() {
    $("#clearImage").val(1);
  }

  window.editName = editName;
  window.updateName = updateName;
  window.cancelName = cancelName;
  window.editDCA = editDCA;
  window.updateDCA = updateDCA;
  window.cancelDCA = cancelDCA;
  window.cancel_requirement = cancel_requirement;
  window.clearImage = clearImage;
});

/***/ }),

/***/ 1:
/*!***************************************************!*\
  !*** multi ./resources/js/admin.bounty-social.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Volumes/Work/Laravel/Decracy-Contribution/tokenlite_app/resources/js/admin.bounty-social.js */"./resources/js/admin.bounty-social.js");


/***/ })

/******/ });