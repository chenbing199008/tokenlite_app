const mix = require("laravel-mix");
const path = require("path");

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
  .setPublicPath("../public")
  // JS
  .js("resources/js/admin.chart.js", "assets/js")
  .js("resources/js/admin.bounty-social.js", "assets/js")
  .js("resources/js/user.bounty.js", "assets/js")
  // Style
  .sass("resources/sass/style.scss", "assets/css")
  .sass("resources/sass/bounty.scss", "assets/css")
  .sass("resources/sass/custom.scss", "css");
