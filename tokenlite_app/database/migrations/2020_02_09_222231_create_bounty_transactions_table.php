<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBountyTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bounty_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('bnt_id');
            $table->string('bnt_type');
            $table->integer('social');
            $table->dateTime('bnt_time');
            $table->double('tokens')->default(0);
            $table->double('bonus_on_base')->default(0);
            $table->double('bonus_on_token')->default(0);
            $table->double('total_bonus')->default(0);
            $table->double('total_tokens');
            $table->integer('stage');
            $table->integer('user');
            $table->double('amount')->nullable();
            $table->string('status')->default('');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bounty_transactions');
    }
}
