<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFollowerPrefixToBountyCountRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bounty_count_rules', function (Blueprint $table) {
            $table->string('follower_prefix')->nullable();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bounty_count_rules', function (Blueprint $table) {
            $table->dropColumn('follower_prefix');
        });
    }
}
