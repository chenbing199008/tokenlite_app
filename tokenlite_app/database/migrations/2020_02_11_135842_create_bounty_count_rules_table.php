<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBountyCountRulesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bounty_count_rules', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('social');
            $table->integer('followers')->nullable(0);
            $table->integer('likes')->nullable(0);
            $table->integer('retweets')->nullable(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bounty_count_rules');
    }
}
