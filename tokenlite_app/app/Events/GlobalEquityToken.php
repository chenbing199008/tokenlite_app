<?php

namespace App\Events;

use App\Models\KYC;
use App\Models\Setting;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Queue\SerializesModels;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;

class GlobalEquityToken
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    const TNX_ID_GLOBAL = 'GLB';

    protected $codeIndex = 0;

    public function __construct()
    {
        $countries = Setting::getValue('global_equity_eligible_countries');

//        $countries = [];

        if (!$countries) {
            Transaction::cancelAllGlobalTnx();
            return true;
        }

        // check whether user is in setting global counties
        $countries = \GuzzleHttp\json_decode($countries, true);

        $activeUsers = User::where('email_verified_at', '<>', null)
            ->whereIn('nationality', $countries)
            ->select(['id'])
            ->get();

        dd($activeUsers);


        $kycActiveUsers = KYC::where(['status' => 'approved'])
            ->whereIn('country', $countries)
            ->select(['gender', 'userId'])
            ->get();

        // no any user in setting countries
        if (!$kycActiveUsers->count()) {
            return true;
        }

        $femaleWeight = Setting::getValue('female_weight');
        if (!$femaleWeight) {
            $femaleWeight = 1.25;
        }

        $maleWeight = Setting::getValue('male_weight');
        if (!$maleWeight) {
            $maleWeight = 1;
        }

        //female weight always more than male weight
        $diffWeight = $femaleWeight - $maleWeight;
        if ($diffWeight < 0) {
            return true;
        }

        $now = date('Y-m-d H:i:s', time());

        $femaleUserIds = [];
        $maleUserIds = [];
        foreach ($kycActiveUsers as $kycActiveUser) {
            $gender = $kycActiveUser->gender;
            if ($gender === 'female') {
                $femaleUserIds[] = $kycActiveUser->userId;
            }

            if ($gender === 'male') {
                $maleUserIds[] = $kycActiveUser->userId;
            }
        }

        if (!$femaleUserIds && !$maleUserIds) {
            return true;
        }

        $totalToken = Setting::getValue('global_equity_amount');
        $femaleUserNumber = count($femaleUserIds);
        $maleUserNumber = count($maleUserIds);

        // only female
        if ($femaleUserIds && !$maleUserIds) {

            Transaction::removeAllGlobalTnx();

            $eachOneGetTokenNumber = $totalToken / $femaleUserNumber;
            $this->createTransactions($femaleUserIds, $eachOneGetTokenNumber, $now);
            return true;
        }

        // only male
        if (!$femaleUserIds && $maleUserIds) {

            Transaction::removeAllGlobalTnx();

            $eachOneGetTokenNumber = $totalToken / $maleUserNumber;
            $this->createTransactions($maleUserIds, $eachOneGetTokenNumber, $now);
            return true;
        }

        // we should remove all GLB transaction
        Transaction::removeAllGlobalTnx();

        $halfToken = $totalToken / 2;
        $femaleAddToken = $diffWeight * $halfToken;
        $femaleAbleGetToken = $halfToken + $femaleAddToken;
        $eachOneGetTokenNumber = $femaleAbleGetToken / $femaleUserNumber;
        $this->createTransactions($femaleUserIds, $eachOneGetTokenNumber, $now);


        // male get token
        $maleAbleGetToken = $totalToken - $femaleAbleGetToken;
        $eachOneGetTokenNumber = $maleAbleGetToken / $maleUserNumber;
        $this->createTransactions($maleUserIds, $eachOneGetTokenNumber, $now);

        return true;
    }

    protected function createTransactions($users, $token, $now)
    {
        $token = $this->formatGetToken($token);

        foreach ($users as $index => $userId) {
            ++$this->codeIndex;
            // create transaction
//            var_dump($index);
//            var_dump($this->createTnxId());

            Transaction::create([
                'tnx_id' => $this->createTnxId(),
                'tnx_type' => 'global',
                'tnx_time' => $now,
                'tokens' => $token,
                'total_tokens' => $token,
                'stage' => 0,
                'user' => $userId,
                'added_by' => 'Global Equity System',
                'status' => 'pending',
                'payment_method' => 'manual',
                'amount' => 0,
            ]);
        }
    }

    protected function formatGetToken($token)
    {
        if (!is_int($token)) {
            return round($token, 1);
        }

        return $token;
    }

    protected function createTnxId()
    {
        return self::TNX_ID_GLOBAL . str_pad($this->codeIndex, 6, 0, STR_PAD_LEFT);
    }

}
