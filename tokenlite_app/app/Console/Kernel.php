<?php

namespace App\Console;

use App\Events\GlobalEquityToken;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use Twitter;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
//        $schedule->call(function () {
//            $bounties = DB::table('bounties')->select('id', 'social_username', 'social_id')->get();
//            foreach ($bounties as $bounty) {
//                if ($bounty->social_id == 1) { // Twitter
//                    $data = Twitter::getFriendShips([
//                        'source_screen_name' => $bounty->social_username,
//                        'target_screen_name' => 'DecracyGlobal'
//                    ]);
//                    $following = $data->relationship->source->following;
//                    if ($following == 1) {
//                        $following_status = 1;
//                    } else {
//                        $following_status = 0;
//                    }
//                    DB::table('transactions')->where('bounty', $bounty->id)->update([
//                        'following_status' => $following_status
//                    ]);
//                } else if ($bounty->social_id == 3) { // Facebook
//                    $following_status = 0;
//                    $access_token = env('FACEBOOK_ACCESS_TOKEN');
//                    $app_id = env('FACEBOOK_APP_ID');
//                    $app_secret = env('FACEBOOK_APP_SECRET');
//
//                    $fb = new \Facebook\Facebook([
//                        'app_id' => $app_id,
//                        'app_secret' => $app_secret,
//                        'default_graph_version' => 'v2.10',
//                    ]);
//
//                    $data = $fb->get('/me?fields=likes,short_name,friends,name', $access_token);
//                    $array = $data->getDecodedBody();
//                    $tmp = $array;
//
//                    foreach ($tmp as $arg) {
//                        if ($arg['name'] == "Decracy") {
//                            $following_status = 1;
//                            return;
//                        }
//                    }
//                    DB::table('transactions')->where('bounty', $bounty->id)->update([
//                        'following_status' => $following_status
//                    ]);
//                }
//            }
//        })->everyMinute();

        $schedule->call(function () {
            GlobalEquityToken::dispatch();
//        })->everyThirtyMinutes();
        })->everyMinute();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__ . '/Commands');

        require base_path('routes/console.php');
    }
}
