<?php

namespace App\Http\Controllers\Admin;

use Validator;
use App\Models\BountyRequirement;
use Illuminate\Http\Request;
use PragmaRX\Google2FA\Google2FA;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Models\IcoStage;
use Exception;


class BountyController extends Controller
{
    public function index()
    {
        // dd('a');
        $requirements = DB::table('bounty_requirements')->get();
        $social = DB::table('socials')->get();
        return view('admin.bounty', compact('requirements', 'social'));
    }

    public function stage()
    {
        $query = "SELECT a.*, SUM(b.bounty) as soldout, COUNT(b.user_id) as submitcount FROM socials a LEFT JOIN bounties b ON a.id = b.social_id GROUP BY a.id";
        $socials = DB::select(DB::raw($query));
        return view('admin.bounty-stage', compact( 'socials'));
    }

    public function edit_stage($id)
    {
        $query = "SELECT a.*, SUM(b.bounty) as soldout, COUNT(b.user_id) as submitcount FROM socials a LEFT JOIN bounties b ON a.id = b.social_id WHERE a.id = $id";
        $socials = DB::select(DB::raw($query));
        $social = $socials[0];
        return view('admin.bounty-stage-edit', compact('social'));
    }

    public function update_stage(Request $request)
    {
        $ret['msg'] = 'info';
        $ret['message'] = __('messages.nothing');

        $social_id = $request->input('social_id');
        $social_name = $request->input('social_name');
        $total_bounty_issue = $request->input('total_bounty_issue');
        $base_dca = $request->input('base_dca');
        $re_start_date = $request->input('start_date');
        $re_start_time = $request->input('start_time');
        $re_end_date = $request->input('end_date');
        $re_end_time = $request->input('end_time');
        $sale_pause = $request->input('sale_pause');

        if ($sale_pause == "on") {
            $status = 1;
        } else {
            $status = 0;
        }

        $start_date = _date($re_start_date.' '.$re_start_time, 'Y-m-d H:i:s');
        $end_date = _date($re_end_date.' '.$re_end_time, 'Y-m-d H:i:s');

        // dd($social_name, $total_bounty_issue, $base_dca, $start_date, $end_date, $sale_pause);
        $res = DB::table('socials')->where('id', $social_id)->update([
            'name' => $social_name,
            'dca' => $base_dca,
            'status' => $status,
            'start_date' => $start_date,
            'end_date' => $end_date,
            'total_bounty_issue' => $total_bounty_issue
        ]);

        if ($res) {
            $ret['msg'] = 'success';
            $ret['message'] = __('messages.update.success', ['what' => 'Bounty Stage']);
        } else{
            $ret['msg'] = 'warning';
            $ret['message'] = __('messages.update.failed', ['what' => 'Bounty Stage']);
        }

        if ($request->ajax()) {
            return response()->json($ret);
        }
        return back()->with([$ret['msg'] => $ret['message']]);
    }

    public function bounty_overview(Request $request)
    {
        $id = $request->get('social_id');
        $query = "SELECT a.*, SUM(b.bounty) as soldout, COUNT(b.user_id) as submitcount FROM socials a LEFT JOIN bounties b ON a.id = b.social_id WHERE a.id = $id";
        $socials = DB::select(DB::raw($query));
        $social = $socials[0];

        // $res = $social->getDecodedBody();
        return response((array)$social);
    }

    public function social(Request $request)
    {
        $social_id = $request->get('id');
        $social = DB::table('socials')->where('id', $social_id)->first();
        $requirements = DB::table('bounty_requirements')->where('social_id', $social_id)->get();

        $rule = DB::table('bounty_count_rules')->where('social', $social_id)->first();

        return view('admin.bounty-social', compact('social', 'requirements', 'rule', 'social_id'));
    }

    public function update(Request $request)
    {
        $socialId = $request->get('social_id');
        $socialName = $request->get('social_name');
        $socialGroup = $request->get('social-group');
        $img = $request->file('image');
        
        $followers_count = $request->get('followers_count');
        $likes_count = $request->get('likes_count');
        $retweets_count = $request->get('retweets_count');
        
        $follower_count_suffix = $request->get('follower_count_suffix');
        $follower_count_prefix = $request->get('follower_count_prefix');
        $likes_count_suffix = $request->get('likes_count_suffix');
        $likes_count_prefix = $request->get('likes_count_prefix');
        $retweets_count_suffix = $request->retweets_count_suffix;
        $retweets_count_prefix = $request->retweets_count_prefix;

        $requirements = $request->input('updated_requirement');

        $clearImage = $request->get('clearImage');

        if ($socialId == 1) {
            try {
                DB::table('bounty_count_rules')->where('id', $socialId)->update([
                    'follower_suffix' => $follower_count_suffix,
                    'followers' => $followers_count,
                    'follower_prefix' => $follower_count_prefix,
                    'likes_suffix' => $likes_count_suffix,
                    'likes' => $likes_count,
                    'likes_prefix' => $likes_count_prefix,
                    'retweets_suffix' => $retweets_count_suffix,
                    'retweets' => $retweets_count,
                    'retweets_prefix' => $retweets_count_prefix,
                ]);
            } catch(Exception $e) {
                dd($e);
            }
        }

        if ($socialId == 3) {
            try {
                DB::table('bounty_count_rules')->where('id', $socialId)->update([
                    'follower_suffix' => $follower_count_suffix,
                    'followers' => $followers_count,
                    'follower_prefix' => $follower_count_prefix,
                ]);
            } catch(Exception $e) {
                dd($e);
            }
        }



        DB::table('bounty_requirements')->where('social_id', $socialId)->delete();
        if ($requirements != null) {
            foreach ($requirements as $item) {
                DB::table('bounty_requirements')->insert([
                    'social_id' => $socialId,
                    'requirement' => $item,
                ]);
            }
        }

        if ($clearImage == 1) {
            DB::table('socials')->where('id', $socialId)->update([
                'image' => '',
            ]);        
        } else {
            if ($img != null) {
                $fileext = $img->getclientOriginalExtension();
                $fileName = $socialName . '.' . $fileext;
                Storage::disk('public_uploads')->put($fileName, File::get($img));
                DB::table('socials')->where('id', $socialId)->update([
                    'image' => $fileName,
                ]);
            }
        }


        DB::table('socials')->where('id', $socialId)->update([
            'name' => $socialName,
            'group' => $socialGroup
        ]);
        return redirect()->route('admin.bounty.setting');
    }
}
