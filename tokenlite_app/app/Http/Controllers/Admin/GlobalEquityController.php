<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\PaymentMethod;
use Illuminate\Http\Request;
use App\Models\Setting;
use IcoHandler;

class GlobalEquityController extends Controller
{
    protected $handler;

    public function __construct(IcoHandler $handler)
    {
        $this->handler = $handler;
    }

    /**
     * Display Global Equity  Settings
     */
    public function settings()
    {
        $countries = $this->handler->getCountries();
        $pm_gateways = PaymentMethod::Currency;
        $globalEquityAmount = Setting::getValue('global_equity_amount');
        $femaleWeight = Setting::getValue('female_weight');
        $maleWeight = Setting::getValue('male_weight');
        $standardWeight = Setting::getValue('standard_weight');
        $disqualifyValue = Setting::getValue('disqualify_value');
        $eligibleCountries = Setting::getValue('global_equity_eligible_countries');
        $globalEndDate = Setting::getValue('global_end_date');
        if (!$eligibleCountries) {
            $eligibleCountries = [];
        } else {
            $eligibleCountries = \GuzzleHttp\json_decode($eligibleCountries, true);
        }

        return view('admin.globalequity-setting', compact(
                'pm_gateways',
                'countries',
                'globalEquityAmount',
                'femaleWeight',
                'maleWeight',
                'standardWeight',
                'eligibleCountries',
                'disqualifyValue',
                'globalEndDate'
            )
        );
    }

    /**
     * Update Global Equity Settings
     */
    public function update_settings(Request $request)
    {
        $type = $request->input('req_type');
        $ret['msg'] = 'info';
        $ret['message'] = __('messages.nothing');

        if ($type == 'global_equity_details') {

            if ($request->input('global_equity_amount') != null) {
                Setting::updateValue('global_equity_amount', $request->input('global_equity_amount'));
            }
            if ($request->input('female_weight') != null) {
                Setting::updateValue('female_weight', $request->input('female_weight'));
            }
            if ($request->input('male_weight') != null) {
                Setting::updateValue('male_weight', $request->input('male_weight'));
            }
            if ($request->input('standard_weight') != null) {
                Setting::updateValue('standard_weight', $request->input('standard_weight'));
            }
            if ($request->input('disqualify_value') != null) {
                Setting::updateValue('disqualify_value', $request->input('disqualify_value'));
            }

            if ($request->input('global_end_date') != null) {
                Setting::updateValue('global_end_date', $request->input('global_end_date'));
            }

            $ret['msg'] = 'success';
            $ret['message'] = __('messages.update.success', ['what' => 'Global Distribution Details']);

        } elseif ($type == 'global_equity_eligible') {
            if ($request->input('countries') != '') {
                Setting::updateValue('global_equity_eligible_countries', \GuzzleHttp\json_encode($request->input('countries')));
            }
            $ret['msg'] = 'success';
            $ret['message'] = __('messages.update.success', ['what' => 'Global Distribution Eligible Countries']);
        } elseif ($type == 'user_panel') {
            //v1.1.0 -> v1.1.2
            $wallet_opt = json_encode(array('wallet_opt' => $request->token_wallet_opt));
            Setting::updateValue('token_wallet_opt', $wallet_opt);

            $wallet_opt_custom = json_encode(array('cw_name' => $request->token_wallet_custom[0], 'cw_text' => $request->token_wallet_custom[1]));
            Setting::updateValue('token_wallet_custom', $wallet_opt_custom);

            if ($request->input('token_wallet_note') != null) {
                Setting::updateValue('token_wallet_note', $request->input('token_wallet_note'));
            }
            Setting::updateValue('token_wallet_req', (isset($request->token_wallet_req) ? 1 : 0));

            //v1.1.2
            if ($request->input('user_in_cur1') != '') {
                Setting::updateValue('user_in_cur1', $request->input('user_in_cur1'));
            }
            if ($request->input('user_in_cur2') != '') {
                Setting::updateValue('user_in_cur2', $request->input('user_in_cur2'));
            }
            Setting::updateValue('user_mytoken_page', (isset($request->user_mytoken_page) ? 1 : 0));
            Setting::updateValue('user_mytoken_stage', (isset($request->user_mytoken_stage) ? 1 : 0));
            //v1.2.0
            Setting::updateValue('kyc_opt_hide', (isset($request->kyc_opt_hide) ? 1 : 0));
            Setting::updateValue('user_sales_progress', (isset($request->user_sales_progress) ? 1 : 0));
            Setting::updateValue('welcome_img_hide', (isset($request->welcome_img_hide) ? 1 : 0));

            $ret['msg'] = 'success';
            $ret['message'] = __('messages.update.success', ['what' => 'User Panel Settings']);
        }

        if ($request->ajax()) {
            return response()->json($ret);
        }
        return back()->with([$ret['msg'] => $ret['message']]);
    }
}
