<?php

namespace App\Http\Controllers\User;

/**
 * Bounty Controller
 *
 *
 * @package TokenLite
 * @author Softnio
 * @version 1.0.6
 */

use Auth;
use Validator;
use IcoHandler;
use Carbon\Carbon;
use App\Models\Page;
use App\Models\User;
use App\Models\BountyRequirement;
use App\Notifications\KycStatus;
use App\Models\DCA;
use App\Models\IcoStage;
use App\Models\UserMeta;
use App\Models\Activity;
use App\Helpers\NioModule;
use App\Models\GlobalMeta;
use App\Models\Transaction;
use App\Models\Social;
use App\Models\BountyTransaction;
use Illuminate\Http\Request;
use PragmaRX\Google2FA\Google2FA;
use App\Notifications\PasswordChange;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

use Exception;

use Twitter;
use Facebook;
use LinkedIn;
use RedditAPI;
use Telegram\Bot\Api;
use RestCord\DiscordClient;

class BountyController extends Controller
{
    protected $handler;
    public function __construct(IcoHandler $handler)
    {
        $this->middleware('auth');
        $this->handler = $handler;
    }

    /**
     * Show the application bounty.
     *
     * @return \Illuminate\Http\Response
     * @version 1.0.0
     * @since 1.0
     * @return void
     */

    public function index()
    {

        $user = Auth::user();

        $requirements = DB::table('bounty_requirements')->get();
        // $dca = DB::table('d_c_as')->get();
        $social = DB::table('socials')->get();

        $query = "SELECT social_id, SUM(bounty) as bounty, socials.name, socials.image, socials.status, socials.dca from bounties LEFT JOIN socials ON social_id = socials.id Group by social_id Order by bounty DESC limit 4";
        $modal_social = DB::select(DB::raw($query));
        $twitter_rule = DB::table('bounty_count_rules')->where('social', 1)->first();
        $facebook_rule = DB::table('bounty_count_rules')->where('social', 3)->first();

        return view('user.bounty', compact('user', 'requirements', 'social', 'modal_social', 'twitter_rule', 'facebook_rule'));
    }

    public function socialUsername(Request $request)
    {
        $social_username = $request->get('username');
        $social_name = $request->get('social_name');
        
        $social = DB::table('socials')->where('name', $social_name)->first();
        
        $retweet_count = 0;
        $likes_count = 0;
        $follower_count = 0;
        if ($social_name == "Twitter") {
            try {
                $user = Twitter::getUsers([
                    'screen_name' => $social_username
                ]);
                $data = Twitter::getFriendShips([
                    'source_screen_name' => $social_username,
                    'target_screen_name' => $social->group
                ]);

                $favorites = Twitter::getFavorites([
                    'screen_name' => $social_username
                ]);

                $userTimeline = Twitter::getUserTimeline([
                    'screen_name' => $social_username
                ]);

                if (isset($favorites)) {
                    foreach($favorites as $obj) {
                        if (isset($obj->user)) {
                            if ($obj->user->screen_name == $social->group) {
                                $likes_count++;
                            }
                        }
                    }
                }

                if (isset($user->followers_count)) {
                    $follower_count = $user->followers_count;
                } else {
                    $follower_count = 0;
                }

                if (isset($userTimeline)) {
                    foreach($userTimeline as $obj) {
                        if (isset($obj->retweeted_status)) {
                            if (isset($obj->retweeted_status->user)) {
                                if ($obj->retweeted_status->user->screen_name == $social->group) {
                                    $retweet_count ++;
                                }
                            }
                        }
                    }
                }

                if (isset($user->screen_name)) {
                    $screen_name = $user->screen_name;
                } else {
                    $screen_name = "error";
                }
            } catch (Exception $e) {
                $ret['msg'] = 'error';
                $ret['message'] = 'Something went wrong!';
            }
            return response()->json(array('data' => $data, 'followers_count' => $follower_count, 'likes_count' => $likes_count, 'retweet_count' => $retweet_count, 'screen_name' => $screen_name));

        } else if ($social_name == "Reddit") {
            try {
                if (!defined('STDIN'))  define('STDIN',  fopen('php://stdin',  'rb'));
                if (!defined('STDOUT')) define('STDOUT', fopen('php://stdout', 'wb'));
                if (!defined('STDERR')) define('STDERR', fopen('php://stderr', 'wb'));

                $abc = RedditAPI::getMe();
                dd($abc);
            } catch (Exception $e) {
                dd($e);
            }
        }else if ($social_name == "LinkedIn") {
            $app_id = env('LINKEDIN_KEY');
            $app_secret = env('LINKEDIN_SECRET');

            // $linkedIn = new Happyr\LinkedIn\LinkedIn('app_id', 'app_secret');
            $scope = array('rw_groups', 'r_contactinfo', 'r_fullprofile', 'w_messages');

            // $url = LinkedIn::getLoginUrl(array('scope' => $scope));
            $user=LinkedIn::get('v1/people/~:(Alfred, Tabaco)');

            dd($user);
        } else if ($social_name == "Discord") {
            $flag = 0;
            $channel_id = (int)$social->group; //general channel
            $bot_token = env('DISCORD_BOT_TOKEN');
            try {
                $discord = new DiscordClient([
                    'token' => $bot_token
                ]);
                $channel = $discord->channel->getChannel(['channel.id' => $channel_id]);
                $guild_id = $channel->guild_id; 
                $guild_members = $discord->guild->listGuildMembers(['guild.id' => $guild_id, 'limit' => 25]);
                
                if (isset($guild_members)) {
                    foreach($guild_members as $member) {
                        if (isset($member->user)) {
                            if ($member->user->id == $social_username) {
                                $flag = 1;
                            }
                        } else {
                            $flag = 0;
                            $ret['msg'] = 'error';
                            $ret['message'] = 'Something went wrong!';
                        }
                    }
                } else {
                    $flag = 0;
                    $ret['msg'] = 'error';
                    $ret['message'] = 'Something went wrong!';
                }
            } catch (Exception $e) {
                $flag = 0;
                $ret['msg'] = 'error';
                $ret['message'] = 'Something went wrong!';
            }
            return response($flag);
        } else {
            // Telegram
            $flag = 0;
            $bot_token = env('TELEGRAM_BOT_TOKEN');
            $chat_id = "@".$social->group;

            $telegram = new Api($bot_token);
            try {
                $res = $telegram->getChatMember([
                    'chat_id' => $chat_id,
                    'user_id' => (int)$social_username
                ]);

                if(isset($res)){
                    if($res->getDecodedBody()["result"]["status"] == "left") {
                        $flag = 0;
                    } else {   // Administrator or Member
                        $flag = 1;
                    }
                }
            } catch (Exception $e) {
                $flag = 0;
                $ret['msg'] = 'error';
                $ret['message'] = 'Something went wrong!';
            }
            return response($flag);
        }
    }

    public function submit(Request $request)
    {
        $msg = '';

        $user = Auth::user();
        $socialId = $request->get('social_id');
        $social_username = $request->get('social_username');
        $firstName = $request->get('firstName');
        $lastName = $request->get('lastName');
        $follow_count = $request->get('current_follow_count');
        $friend_count = $request->get('current_friends_count');
        $likes_count = $request->get('likes-count');
        $retweet_count = $request->get('retweet-count');
        $url = $request->get('social_url');

        try {
            $bounty = DB::table('socials')->where('id', $socialId)->first();
            $dca = $bounty->dca;

            DB::table('bounties')->insert([
                'user_id' => $user->id,
                'first_name' => $firstName,
                'last_name' => $lastName,
                'social_id' => $socialId,
                'social_username' => $social_username,
                'follow_count' => $follow_count,
                'friend_count' => $friend_count,
                'likes_count' => $likes_count,
                'retweet_count' => $retweet_count,
                'bounty' => $dca,
                'url' => $url
            ]);

            $bounty_latest_id = DB::getPdo()->lastInsertId();

            // Add BountyTransaction

            $cur_time = Carbon::now();
            $bnt_time = _date($cur_time, 'Y-m-d H:i:s');

            $last_bnt = DB::table('transactions')->where('tnx_type', 'bounty')->orderBy('id', 'desc')->first();
            $bnt_id = 1;
            if ($last_bnt == null) {
                $bnt_id = 1;
                $bnt_id = str_pad($bnt_id, 6, '0', STR_PAD_LEFT);
            } else {
                $last_bnt_id = substr($last_bnt->tnx_id, 3);
                $bnt_new_id = (int) $last_bnt_id + 1;
                $bnt_id = str_pad($bnt_new_id, 6, '0', STR_PAD_LEFT);
            }
            $social_name = DB::table('socials')->where('id', $socialId)->select('name')->first();
            $ico_stage = DB::table('ico_stages')->where('id', 1)->select('base_price')->first();
            $base_price = $ico_stage->base_price;

            try {
                $result = DB::table('transactions')->insert([
                    'tnx_id' => 'BNT' . $bnt_id,
                    'tnx_type' => 'bounty',
                    'tnx_time' => "2020-02-06 23:09:34",
                    'tokens' => $dca,
                    'bonus_on_base' => 0,
                    'bonus_on_token' => 0,
                    'total_bonus' =>  0,
                    'total_tokens' => $dca,
                    'stage' => 0,
                    'user' => $user->id,
                    'amount' => 0,
                    'receive_amount' => 0,
                    'receive_currency' => "dca",
                    'base_amount' => $dca * $base_price,   // token * base_currency_rate
                    'base_currency' => "usd",
                    'base_currency_rate' => $base_price,
                    'currency' => 'usd',
                    'currency_rate' => 0,
                    'all_currency_rate' => '',
                    'wallet_address' => '',
                    'payment_method' => 'manual',
                    'payment_id' => '',
                    'payment_to' => $firstName . $lastName,
                    'checked_by' => '',
                    'added_by' => $user->email,
                    'checked_time' => $bnt_time,
                    'details' => 'bounty distribution',
                    'extra' => '',
                    'status' => 'pending',
                    'dist' => 0,
                    'refund' => 0,
                    'social' => $social_name->name,
                    'bounty' => $bounty_latest_id,
                    'following_status' => 1,
                    'pending' => 1,
                    'friends_count' => $friend_count
                ]);

                // $result = DB::table('facebooks')->insert([
                //     'name' => 'abc'
                // ]);

                // if ($result) {
                //     try {
                //         $user->notify(new BountyStatus($result));
                //     } catch(\Exception $e){
                //         $ret['msg'] = 'success';
                //         $ret['message'] = __('messages.bounty.forms.submitted');
                //         $ret['link'] = route('bounty') . '?thank_you=true';
                //     }
                // }
            } catch (Exception $e) {
                $msg = __('messages.somthing_wrong');
                return back()->with([$ret['msg'] => $ret['message']]);
            }

            return redirect()->route('bounty');
        } catch (Exception $e) {
            $msg = __('messages.somthing_wrong');
            return back()->with([$ret['msg'] => $ret['message']]);
        }
    }

    public function facebook_callback() {
        $group = DB::table('socials')->where('id', 3)->first();

        $app_id = env('FACEBOOK_APP_ID');
        $app_secret = env('FACEBOOK_APP_SECRET');
        $fb = new Facebook\Facebook([
            'app_id' => $app_id,
            'app_secret' => $app_secret,
            'default_graph_version' => 'v2.2',
        ]);
        
        $helper = $fb->getJavaScriptHelper();
        
        try {
            $accessToken = $helper->getAccessToken();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
        
        if (! isset($accessToken)) {
            echo 'No cookie set or no OAuth data could be obtained from cookie.';
            exit;
        }
        
        $res = $fb->get('/me?fields=likes,short_name,friends,name,link', $accessToken->getValue());
        $data = $res->getDecodedBody();
        $likes_data = $data["likes"]["data"];

        $likes = 0;
        $friends = 0;
        $link = "";
        $username = "";
        if (isset($likes_data)) {
            foreach($likes_data as $obj){
                if($obj["name"] == $group->group) {
                    $likes++;
                }
            }
        }

        if (isset($data["friends"])) {
            if (isset($data["friends"]["summary"]["total_count"])) {
                $friends = $data["friends"]["summary"]["total_count"];
            }
        }

        if (isset($data["link"])) {
            $link = $data["link"];
        }

        if (isset($data["name"])) {
            $username = $data["name"];
        }

        $response = array("likes_count"=>$likes, "friends_count" => $friends, "profile_link" => $link, "username" => $username);

        // $_SESSION['fb_access_token'] = (string) $accessToken;

        // var_dump($data);

        return response($response);
    }
}
