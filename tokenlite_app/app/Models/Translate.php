<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Translate
 *
 * @property int $id
 * @property string $key
 * @property string $name
 * @property string|null $text
 * @property string $pages
 * @property string $group
 * @property string $panel
 * @property int $load
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate whereGroup($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate whereKey($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate whereLoad($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate wherePages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate wherePanel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate whereText($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Translate whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Translate extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'translates';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['key', 'name', 'text', 'pages', 'group', 'panel', 'load'];
}
