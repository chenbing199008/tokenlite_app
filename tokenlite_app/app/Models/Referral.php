<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Referral
 *
 * @property int $id
 * @property int|null $user_id
 * @property int|null $user_bonus
 * @property int|null $refer_by
 * @property int|null $refer_bonus
 * @property string|null $meta_data
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Referral newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Referral newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Referral query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Referral whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Referral whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Referral whereMetaData($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Referral whereReferBonus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Referral whereReferBy($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Referral whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Referral whereUserBonus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Referral whereUserId($value)
 * @mixin \Eloquent
 */
class Referral extends Model
{
    /*
     * Table Name Specified
     */
    protected $table = 'referrals';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'user_bonus', 'refer_by', 'refer_bonus', 'meta_data'
    ];
}
