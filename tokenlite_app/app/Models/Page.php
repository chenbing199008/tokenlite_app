<?php
/**
 * Page Model
 *
 *  Manage the Pages
 *
 * @package TokenLite
 * @author Softnio
 * @version 1.0
 */
namespace App\Models;

use IcoData;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Page
 *
 * @property int $id
 * @property string $title
 * @property string $menu_title
 * @property string $slug
 * @property string $custom_slug
 * @property string|null $meta_keyword
 * @property string|null $meta_description
 * @property int $meta_index
 * @property string $description
 * @property string|null $external_link
 * @property string $status
 * @property string $lang
 * @property int $public
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereCustomSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereExternalLink($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereLang($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereMenuTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereMetaDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereMetaIndex($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereMetaKeyword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page wherePublic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Page whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Page extends Model
{

    /*
     * Table Name Specified
     */
    protected $table = 'pages';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'menu_title', 'slug', 'custom_slug', 'description', 'status', 'lang', 'public',
    ];

    /**
     *
     * Get the page
     *
     * @version 1.0.0
     * @since 1.0
     * @return void
     */
    public static function get_page($slug, $get = '')
    {
        $data = self::where('slug', $slug)->first();
        if ($data == null) {
            $data = IcoData::default_pages($slug);
        }
        if ($get == '') {
            $res = ($data ? $data : null);
        } else {
            $res = $data->$get ? $data->$get : null;
        }
        return $res;
    }

    /**
     *
     * Get slug
     *
     * @version 1.0.0
     * @since 1.0
     * @return void
     */
    public static function get_slug($slug)
    {
        $data = self::where('slug', $slug)->first();
        if ($data == null) {
            $data = IcoData::default_pages($slug);
        } else {
            return $data->custom_slug;
        }

        return $slug;
    }
}
