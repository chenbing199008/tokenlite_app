<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
/**
 * App\Models\BountyTransaction
 *
 * @property int $id
 * @property string $field
 * @property string|null $value
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereField($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Setting whereValue($value)
 * @mixin \Eloquent
 * */

class BountyTransaction extends Model
{
    protected $table = 'bounty_transactions';
    protected $fillable = ['bnt_id', 'bnt_type', 'social', 'bnt_time', 'tokens', 'bonus_on_base', 'bonus_on_token', 'total_bonus', 'total_tokens', 'stage', 'user', 'status'];

        /**
     *
     * Relation with user
     *
     * @version 1.0.1
     * @since 1.0
     * @return void
     */

    public function bntUser()
    {
        return $this->belongsTo(User::class, 'user', 'id');
    }

       /**
     *
     * Relation with auth user
     *
     * @version 1.0.0
     * @since 1.1.2
     * @return void
     */


    public function user_tnx()
    {
        return $this->belongsTo(User::class, 'user', 'id')->where('user', auth()->id());
    }
}
