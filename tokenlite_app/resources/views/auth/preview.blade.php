<div class="page-intro-side page-intro-side-b">
    <div class="text-block text-center wide-max-xs wide-auto">
        <h2 class="page-intro-title"><span>Contributor/Investor Panel</span> <small class="text-dark fs-14 ucap">User Dashboard</small></h2>
        <p class="lead">Register with your <strong>Real Email</strong> address as a <strong>Contributor or Investor</strong> and check out User Panel.</p>
        <div class="gaps-4x"></div>
        <a target="_blank" href="https://app.tokenwiz.xyz/register" class="btn btn-lg btn-primary"><span>Register as Contributor</span></a>
        <div class="gaps-2x"></div>
        <div class="hint hint-secondary">* We don't share email address with anyone.</div>
    </div>
</div>
