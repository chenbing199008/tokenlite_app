@extends('layouts.admin')
@section('title', __('Bounty'))
@section('content')
@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/bounty.css') }}">
@endsection
@push('header')
<style>
    .col-10 {
        max-width: 80%;
    }
    .col-4 {
        margin-left: 5px;
        margin-right: 5px;
    }
    .minimum-follower {
        display: flex;
        justify-content: space-between;
    }
    .minimum-follower>span {
        margin-top: 7px;
    }

    .upload-btn-wrapper {
        position: relative;
        overflow: hidden;
        display: inline-block;
    }

    .upload-btn {
        border: 2px solid gray;
        color: gray;
        background-color: white;
        padding: 8px 20px;
        border-radius: 8px;
        font-size: 13px;
        font-weight: bold;
    }

    .upload-btn-wrapper input[type=file] {
        font-size: 100px;
        position: absolute;
        left: 0;
        top: 0;
        opacity: 0;
    }

    .upload-image {
        display: flex;
        justify-content: space-between;
    }
</style>
@endpush
<div class="page-content">
    <div class="container">
        <div class="bounty">
            <div class="row">
                <div class="justify-content-center align-items-end" style="padding-left: 17rem;">
                    <div class="bounty_header">
                        <div style="width:210px; height:210px;">
                            <a href="{{ route('bounty') }}">
                                <img src="/images/fantasy_characters/planets.svg" class="space_icon"/>
                            </a>
                        </div>
                        <span class="bounty_title">{{__('Bounty')}}</span>
                    </div>
                </div>
                <div class="row" style="text-align: right; align-items: flex-end;">
                    <div class="image-hover-zoom">
                        <img src="/images/fantasy_characters/477154.svg" />
                    </div>
                    <div class="image-hover-zoom">
                        <img src="/images/fantasy_characters/477187.svg"/>
                    </div>
                    <div class="image-hover-zoom">
                        <img src="/images/fantasy_characters/477179.svg"/>
                    </div>
                </div>
            </div>
        </div>

        <form action="{{ route('admin.ajax.bounty.social.update') }}" class="validate-modern" method="POST" id="update_page" enctype="multipart/form-data">
            @csrf
            <input type="hidden" value="{{$social->id}}" name="social_id" />

            <div class="card-innr">
                <div class="card">
                    <div class="card-header">
                        Social Name
                    </div>
                    <div class="card-body">
                        <div class="input-group mb-3">
                            <input type="text" class="form-control socialName" value="{{$social->name}}" name="social_name"/>
                        </div>
                        <a onclick="editName()" class="btn btn-primary" id="editNamebtn"><span>Edit</span></a>
                        <div class="d-flex justify-content-end">
                            <a onclick="updateName()" class="btn btn-primary" id="updateNamebtn"><span>Ok</span></a>
                            <a onclick="cancelName('{{$social->name}}')" class="btn btn-outline-secondary ml-3" id="cancelNamebtn"><span>Cancel</span></a>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Decracy Social Name --}}
            <div class="card-innr">
                <div class="card">
                    @if ($social->id == 6 || $social->id == 2)
                    <div class="card-header">
                        Channel ID
                    </div>
                    @else
                    <div class="card-header">
                        Social Group Name
                    </div>
                    @endif
                    <div class="card-body">
                        <input type="text" class="form-control col-12" id="social_group" style="height: auto" value="{{$social->group}}" name="social-group" />
                    </div> {{-- Card-body --}}
                </div>
            </div>
            {{-- Requirements --}}
            <div class="card-innr">
                <div class="card">
                    <div class="card-header">
                        Requirements
                    </div>
                    <div class="card-body">
                        <div class="add_text">
                            <input type="text" class="form-control col-10" id="new_requirement" style="height: auto" placeholder="{{ __('Add New Requirement') }}"/>
                            <div class="btn btn-primary" id="add_requirement_btn"><span>Add</span></div>
                        </div>
                        <ul class="list-group mt-5" id="requirement_list">
                            @if($social_id == 1)
                            <div class="requirement_group">
                                <li class="list-group-item mt-3 minimum-follower">
                                    <input type="text" name="follower_count_suffix" style="text-align: center" class="form-control col-4" value="{{ $rule->follower_suffix }}" />
                                    <input type="number" name="followers_count" value= "{{ $rule->followers }}" style="text-align: center" class="form-control col-3" />
                                    <input type="text" name="follower_count_prefix" style="text-align: center" class="form-control col-4" value="{{ $rule->follower_prefix }}" />
                                </li>
                            </div>
                            <div class="requirement_group">
                                <li class="list-group-item mt-3 minimum-follower"> 
                                    <input type="text" name="likes_count_suffix" style="text-align: center" class="form-control col-4" value="{{ $rule->likes_suffix }}" />
                                    <input type="number" name="likes_count" value= "{{ $rule->likes }}" style="text-align: center" class="form-control col-3" />
                                    <input type="text" name="likes_count_prefix" style="text-align: center" class="form-control col-4" value="{{ $rule->likes_prefix }}" />
                                </li>
                            </div>
                            <div class="requirement_group">
                                <li class="list-group-item mt-3 minimum-follower"> 
                                    <input type="text" name="retweets_count_suffix" style="text-align: center" class="form-control col-4" value="{{ $rule->retweets_suffix }}" />
                                    <input type="number" name="retweets_count" value= "{{ $rule->retweets }}" style="text-align: center" class="form-control col-3" />
                                    <input type="text" name="retweets_count_prefix" style="text-align: center" class="form-control col-4" value="{{ $rule->retweets_prefix }}" />
                                </li>
                            </div>
                            @endif
                            @if($social_id == 3)
                            <div class="requirement_group">
                                <li class="list-group-item mt-3 minimum-follower"> 
                                    {{-- Facebook Friends count --}}
                                    <input type="text" name="follower_count_suffix" style="text-align: center" class="form-control col-4" value="{{ $rule->follower_suffix }}" />
                                    <input type="number" name="followers_count" value= "{{ $rule->followers }}" style="text-align: center" class="form-control col-3" />
                                    <input type="text" name="follower_count_prefix" style="text-align: center" class="form-control col-4" value= "{{ $rule->follower_prefix }}" />
                                </li>
                            </div>
                            @endif
                            @foreach ($requirements as $item)
                                <div class="requirement_group">
                                    <li class="list-group-item mt-3"> 
                                        <span class="requirement"> {{ $item->requirement }} </span>
                                        <div class="float-right">
                                            <a class="text-success enableEdit_btn" style="cursor: pointer"><span>Edit</span></a>
                                            <a class="text-danger ml-1 delete_btn" style="cursor: pointer">
                                                <span>Delete</span>
                                            </a>
                                        </div>
                                    </li>

                                    <div class="card mt-5 d-none myCard editcard">
                                        <div class="card-header">
                                            Edit
                                        </div>
                                        <div class="card-body">
                                        <input type="text" class="form-control col updated_requirement" name="updated_requirement[]" value="{{ $item->requirement }}" />
                                            <div class="d-flex justify-content-end mt-3">
                                                <a class="btn btn-primary updateRequirement_btn"><span>OK</span></a>
                                                <a class="btn btn-danger ml-1 cancelRequirement_btn"><span>Cancel</span></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>

            {{-- File Upload --}}
            <div class="card-innr">
                <div class="card">
                    <div class="card-header">
                        Image Upload
                    </div>
                    <div class="card-body upload-image">
                        <div class="upload-btn-wrapper">
                            <button class="upload-btn"><span>Upload a file</span></button>
                            <input type="file" name="image" />
                        </div>
                        <input type="hidden" name="clearImage" id="clearImage" />
                        <a href="#" type="button" onclick="clearImage()"><span class="clearImagetext">Clear</span></a>
                    </div> {{-- Card-body --}}
                </div>
            </div>

            {{--  Submit  Button --}}
            <div class="card-innr">
                <div class="card-body footer-buttons">
                    <button type="submit" class="btn btn-primary"><span>{{ __('Save') }}</span></button>
                    <a type="button" class="btn btn-danger ml-1" href="{{ route('admin.bounty.setting') }}">Cancel</a>
                </div>
            </div>
        </form>
    </div>
</div>
@endsection

@push ('footer')
{{-- Delete Warning Dialog --}}
<div class="modal fade" id="confirm-modal" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Warning</h5>
                <button class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Are you Sure?</p>
            </div>
            <input type="hidden" name="item_id" id="item_id">
            <div class="modal-footer">
                <div class="btn btn-primary yes_btn"><span>Yes</span></div>
                <div class="btn btn-danger" data-dismiss="modal">No</div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('assets/js/admin.bounty-social.js') }}"></script>
@endpush