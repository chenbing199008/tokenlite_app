@extends('layouts.admin')
@section('title', 'Bounty Stage')

@push('header')
<style>
    .dropdown-list li a:hover {
        background: -webkit-linear-gradient(90deg, #50BD8C 0%, #07BCC3 54%, #21BBEE 100%);   
        -webkit-background-clip: text;
        -webkit-text-fill-color: transparent;                                               
    }

    .btn {
        cursor: text !important;
    }
    .btn-primary>span {
        padding: 1.9px 20px !important;
    }
    .btn-primary:hover span{
        background-image: none !important;
        color: black;
    }
    .btn-primary:hover:before{
        opacity: 1 !important;
    }
</style>
@endpush
@section('content')
<div class="page-content">
    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head d-flex justify-content-between align-items-center">
                    <h4 class="card-title mb-0">Bounty Stage</h4>
                </div>
                <div class="gaps-1-5x"></div>
                <div class="row guttar-vr-30px">
                    @forelse($socials as $social)
                    <div class="col-xl-4 col-md-6">
                        <div class="stage-item stage-card {{(gws('actived_stage') == $social->status)?'stage-item-actived':'' }}">
                            <div class="stage-card-body">
                                <div class="stage-head">
                                    <div class="stage-title">
                                        <h6>Bounty Name
                                        @if((date('Y-m-d H:i:s') >= $social->start_date) && (date('Y-m-d H:i:s') <= $social->end_date) && ($social->status != 0))
                                        <span class="badge badge-success">Running</span>
                                        @elseif((date('Y-m-d H:i:s') >= $social->start_date && date('Y-m-d H:i:s') <= $social->end_date) && ($social->status == 0))
                                        <a class="btn btn-sm btn-auto btn-primary badge btn-paused">
                                            <span class="d-none d-sm-inline-block">Paused</span>
                                        </a>
                                        @endif
                                        <h4>{{$social->name}}</h4>
                                    </div>

                                    <div class="stage-action">
                                        <a href="#" class="toggle-tigger rotate"><em class="ti ti-more-alt"></em></a>
                                        <div class="toggle-class dropdown-content dropdown-content-top-left">
                                            <ul class="dropdown-list">
                                                <li>
                                                    <a class="user-email-action" href="#bounty-overview" data-id="{{ $social->id }}" data-title="{{ $social->name }}" data-toggle="modal">Overview</a>
                                                </li>
                                                {{-- <li><a href="javascript:void(0);" data-action="overview" data-view="modal" data-stage="{{$social->id}}">Overview</a></li> --}}
                                                <li><a href="{{route('admin.bounty.stages.edit', $social->id)}}">Update Stage</a></li>
                                            </ul>
                                        </div>
                                    </div>

                                </div>

                                <div class="stage-info stage-info-status">
                                    <div class="stage-info-graph">
                                        @if(!($social->start_date > date('Y-m-d H:i:s') && date('Y-m-d H:i:s') < $social->end_date))
                                        <div class="progress-pie progress-circle">
                                            <input class="knob d-none" data-thickness=".125" data-width="100%" data-fgColor="#07BCC3" data-bgColor="#c8d2e5" value="{{ to_percent($social->soldout, $social->total_bounty_issue) }}">
                                            <div class="progress-txt"><span class="progress-amount">{{ to_percent($social->soldout, $social->total_bounty_issue) }}</span>% <span class="progress-status">Sold</span></div>
                                        </div>
                                        @else
                                        <div class="progress-soon progress-circle">
                                            <div class="progress-txt"><span class="progress-status">Coming Soon</span></div>
                                        </div>
                                        @endif
                                    </div>
                                    <div class="stage-info-txt">
                                        <h6>Bounty Issued</h6>
                                        <span class="stage-info-total h2">{{ to_num_token($social->total_bounty_issue, 0) }}</span>
                                        <div class="stage-info-count">Distributed <span>{{ to_num_token($social->soldout, 0) }}</span> DCA</div>
                                    </div>
                                </div>
                                <div class="stage-info">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="stage-info-txt">
                                                <h6>Submitted Users</h6>
                                                <div class="h2 stage-info-number">{{ $social->submitcount }}</div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="stage-info-txt">
                                                <h6>Bounty Amount</h6>
                                                <div class="h2 stage-info-number">{{ ($social->dca) }}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="stage-date">
                                    <div class="row">
                                        <div class="col-6">
                                            <h6>Start Date</h6>
                                            <h5>{{ _date($social->start_date, gws('site_date_format')) }} <small>{{ _date($social->start_date, gws('site_time_format')) }}</small></h5>
                                        </div>
                                        <div class="col-6">
                                            <h6>End Date</h6>
                                            <h5>{{ _date($social->end_date, gws('site_date_format')) }} <small>{{ _date($social->end_date, gws('site_time_format')) }}</small></h5>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    @empty
                    <span>No Stage Found</span>
                    @endforelse
                </div>
                <div class="gaps-0-5x"></div>
            </div> {{-- card-innr --}} 
        </div> {{-- card content-area --}} 
    </div> {{-- container --}}
</div> {{-- page-content --}}

@endsection


@push ('footer')
<div class="modal fade" id="bounty-overview" tabindex="-1">
    <div class="modal-dialog modal-dialog-md modal-dialog-centered">
        <div class="modal-content">
            <a href="#" class="modal-close" data-dismiss="modal" aria-label="Close"><em class="ti ti-close"></em></a>
            <div class="popup-body popup-body-md">
                <h3 class="popup-title">{{ __('Overview of') }} <span id="title" style="color: black;"></span> </h3>
                <div class="popup-content">
                    <div class="card-bordered nopd">
                        <div class="card-innr">
                            <div class="row guttar-vr-20px align-items-center">
                                <div class="col-sm-6">
                                    <div class="total-block">
                                        <h6 class="total-title ucap">{{ __('Total Bounty Issued') }}</h6>
                                        <span class="total-amount-lead" id="total_bounty_issued"></span>
                                        <p class="total-note">{{ __('In based price') }} <span>{{ to_num((23)).' ' }}</span></p>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="total-block">
                                        <h6 class="total-title ucap">{{ __('Bounty Sold') }}</h6>
                                        <span class="total-amount-lead" id="soldout"></span>
                                        <p class="total-note">{{ __('Unsold') }} <span id="unsold"></span> {{ __('DCA') }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="card-innr">
                            <div class="total-block">
                                <h5 class="total-title-sm">{{ __('In Progress') }}</h5>
                                <span class="total-amount" id="in_progress"></span>
                            </div>
                        </div>
                        <div class="card-innr">
                            <div class="total-block">
                                <h5 class="total-title-sm">{{ __('Total Collected') }}</h5>
                                <span class="total-amount">0</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(function(e) {
        
        $("#bounty-overview").on("shown.bs.modal", function(e) {
            var social_id = $(e.relatedTarget).data('id');
            var social_name = $(e.relatedTarget).data('title');
            $("#title").html(social_name);

            $.ajax({
                async: true,
                type: 'GET',
                dataType: 'json',
                crossDomain: true,
                // url: 'http://tokenlite.local/admin/bounty/overview',
                url: 'https://contribution.decracy.com/admin/bounty/overview',
                data: {
                    'social_id': social_id,
                },
                success: function (data) {
                    var tmp = data.total_bounty_issue - data.soldout;
                    $("#total_bounty_issued").html(data.total_bounty_issue);
                    $("#soldout").html(data.soldout);
                    $("#unsold").html(tmp);
                    $("#in_progress").html(tmp + "&nbsp;" + "DCA");
                    console.log(data);
                },
                error: function() {
                    alert('error');
                },
            });
        });
    });
</script>
@endpush