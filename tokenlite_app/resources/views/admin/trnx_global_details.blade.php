@extends('layouts.admin')
@section('title', 'Transaction Details')
<style>
    .reddot {
        width: 10px;
        height: 10px;
        background-color: red;
        border-radius: 100%;
        margin-top: 5px;
        margin-right: 10px;
    }
    .bluedot {
        width: 10px;
        height: 10px;
        background-color: #4ebc8c;
        border-radius: 100%;
        margin-top: 5px;
        margin-right: 10px;
    }
    .green-icon {
        background: #33BD5B;
        position: absolute;
        height: 16px;
        width: 16px;
        border-radius: 50%;
        content: "";
        top: 0;
        left: 0;
    }
</style>
@section('content')
<div class="page-content">
    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head d-flex justify-content-between align-items-center">
                    <a href="{{ (url()->previous()) ? url()->previous() : route('admin.transactions') }}" class="btn btn-sm btn-auto btn-primary">
                        <span><em class="fas fa-arrow-left"></em><span class="d-sm-inline-block d-none">Back</span></span>
                    </a>
                </div>
                <div class="gaps-1-5x"></div>
                <div class="data-details d-md-flex">
                    <div class="fake-class">
                        <span class="data-details-title">Transaction Date</span>
                        <span class="data-details-info">{{ _date($trnx->tnx_time) }}</span>
                    </div>
                    <div class="fake-class">
                        <span class="data-details-title">Transaction Status</span>
                        <span class="badge badge-{{ __status($trnx->status, 'status') }} ucap">{{ $trnx->status }}</span>
                    </div>
                    <div class="fake-class">
                        <span class="data-details-title">Transaction by</span>
                        <span class="data-details-info"><strong>{{ $trnx->added_by }}</strong></span>
                    </div>
                    <div class="fake-class">
                        @if($trnx->tnx_type=='refund')
                            @php 
                            $trnx_extra = (is_json($trnx->extra, true) ?? $trnx->extra);
                            @endphp
                            <span class="data-details-title">Refund Note</span>
                            <span class="data-details-info">{{ $trnx_extra->message }}</span>
                        @else
                            <span class="data-details-title">Transaction Note</span>
                            @if($trnx->checked_by != NULL)
                            <span class="data-details-info">{{ ucfirst($trnx->status) }} By <strong>{{ ucfirst(approved_by($trnx->checked_by)) }}</strong> <br> at {{ _date($trnx->checked_time) }}</span>
                            @elseif($trnx->status == 'canceled')
                            <span class="data-details-info">Canceled by User</span>
                            @else
                            <span class="data-details-info">Not Reviewed yet.</span>
                            @endif
                        @endif
                    </div>
                </div>
                <div class="gaps-3x"></div>
                <h6 class="card-sub-title">Transaction Info</h6>

                <ul class="data-details-list">
                    <li>
                        <div class="data-details-head">Transaction Type</div>
                        <div class="data-details-des">
                            <span>Global</span>
                        </div>
                    </li>
                </ul>

                <div class="gaps-3x"></div>
                <h6 class="card-sub-title">GLOBAL EQUITY DISTRIBUTION</h6>
                <ul class="data-details-list">
                    <li>
                        <div class="data-details-head">Amount (T)</div>
                        <div class="data-details-des"><span>{{ to_num($trnx->tokens, 'min', '', false) }} {{ token_symbol() }}</span></div>
                    </li>

                    <li>
                        <div class="data-details-head">Distributed To</div>
                        <div class="data-details-des justify-content-start"><strong>{{ set_id($trnx->user, 'user') }}</strong><span>- {{ $trnx->tnxUser->email }}</span></div>
                    </li>

                    <li>
                        <div class="data-details-head">User KYC'd?</div>
                        <div class="data-details-des justify-content-start">
                            @if($trnx->tnxUser->kyc_info)
                                <div class="position-relative"><span class="green-icon"></span><span style="margin-left: 20px">Yes</span></div>
                            @else
                                <span>No</span>
                            @endif
                        </div>
                    </li>

                    <li>
                        <div class="data-details-head">Eligible Country</div>
                        <div class="data-details-des">
                            <span>{{ $country }}</span>
                        </div>
                    </li>

                </ul>
                <div class="gaps-3x"></div>
                <div class="gaps-0-5x"></div>
            </div>
        </div>{{-- .card --}}
    </div>{{-- .container --}}
</div>{{-- .page-content --}}
@endsection

@push ('footer')
<script>
    $(function() {
        function gotoSocial(url) {
            window.open(url, '__blank');
        }
    });
</script>
@endpush
