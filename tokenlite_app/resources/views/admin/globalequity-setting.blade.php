@extends('layouts.admin')
@section('title', 'GLOBAL EQUITY Setting')


@section('content')
    <div class="page-content">
        <div class="container">
            <div class="row">
                <div class="main-content col-lg-12">
                    @include('vendor.notice')
                    <div class="content-area card">
                        <div class="card-innr">
                            <div class="card-head">
                                <h4 class="card-title">Global Equity Settings </h4>
                            </div>
                            <div class="gaps-1x"></div>
                            <div class="card-text ico-setting setting-token-details">
                                <h3 class="card-title-md text-primary">Global Distribution Details</h3>
                                <form action="{{ route('admin.ajax.globalequity.settings.update') }}" method="POST"
                                      id="stage_setting_details_form" class="validate-modern">
                                    @csrf
                                    <input type="hidden" name="req_type" value="global_equity_details">
                                    <div class="row">
                                        <div class="col-xl-3 col-sm-6">
                                            <div class="input-item input-with-label">
                                                <label class="input-item-label">Distribution Amount</label>
                                                <div class="input-wrap">
                                                    <input class="input-bordered" required type="number"
                                                           name="global_equity_amount"
                                                           value="{{ $globalEquityAmount }}" min="1">
                                                </div>
                                                <span class="input-note">Enter the amount of token to be distributed globally.</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-xl-3 col-sm-6">
                                            <div class="input-item input-with-label">
                                                <label class="input-item-label">Female Weight</label>
                                                <div class="input-wrap">
                                                    <input class="input-bordered" type="number" name="female_weight"
                                                           min="1" value="{{ $femaleWeight }}">
                                                </div>
                                                <span class="input-note">Distribution weight of Female users who register globally.</span>
                                            </div>
                                        </div>
                                        <div class="col-xl-3 col-sm-6">
                                            <div class="input-item input-with-label">
                                                <label class="input-item-label">Male Weight</label>
                                                <div class="input-wrap">
                                                    <input class="input-bordered" type="number" name="male_weight"
                                                           min="1" value="{{ $maleWeight }}">
                                                </div>
                                                <span class="input-note">Distribution weight of Male users who register globally.</span>
                                            </div>
                                        </div>
                                        <div class="col-xl-3 col-sm-6">
                                            <div class="input-item input-with-label">
                                                <label class="input-item-label">Standard Weight</label>
                                                <div class="input-wrap">
                                                    <input class="input-bordered" type="number"
                                                           name="standard_weight"
                                                           min="1" value="{{ $standardWeight }}">
                                                </div>
                                                <span class="input-note">Distribution weight of 'Any' users who register globally.</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xl-3 col-sm-6">
                                            <div class="input-item input-with-label">
                                                <label class="input-item-label">Disqualify value</label>
                                                <div class="input-wrap">
                                                    <input class="input-bordered" type="number"
                                                           name="disqualify_value"
                                                           min="1" value="{{ $disqualifyValue }}">
                                                </div>
                                                <span class="input-note">A member is disqualified if they contribute more than this amount.</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-xl-3 col-sm-6">
                                            <div class="input-item input-with-label">
                                                <label class="input-item-label">Global End Date</label>
                                                <div class="input-wrap">
                                                    <input class="input-bordered custom-date-picker" type="text" name="global_end_date" value="{{ stage_date($globalEndDate ) }}">
                                                    <span class="input-icon input-icon-right date-picker-icon"><em class="ti ti-calendar"></em></span>
                                                </div>
                                                <span class="input-note">Global Equity End Date.</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="gaps-1x"></div>
                                    <div class="d-flex">
                                        <button class="btn btn-primary save-disabled" type="submit" disabled>
                                            <span><i class="ti ti-reload"></i><span>Update</span></span>
                                        </button>
                                    </div>
                                </form>
                            </div>

                            <div class="sap sap-gap"></div>
                            <div class="card-text ico-setting setting-token-purchase">
                                <h4 class="card-title-md text-primary">Eligible Countries</h4>
                                <form action="{{ route('admin.ajax.globalequity.settings.update') }}" method="POST"
                                      id="stage_setting_purchase_form" class="validate-modern">
                                    @csrf
                                    <input type="hidden" name="req_type" value="global_equity_eligible">
                                    <div class="row">
                                        <div class="col-lg-3 col-sm-6">
                                            <div class="input-item input-with-label">
                                                <label class="input-item-label">Per Token Price</label>
                                                <div class="input-wrap input-wrap-switch">
                                                    <input class="input-switch" name="token_price_show" type="checkbox"
                                                           {{ token('price_show') == 1 ? 'checked' : '' }} id="per-token-price">
                                                    <label for="per-token-price">Show</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-6">
                                            <div class="input-item input-with-label">
                                                <label class="input-item-label">KYC Before Purchase</label>
                                                <div class="input-wrap input-wrap-switch">
                                                    <input class="input-switch" name="token_before_kyc" type="checkbox"
                                                           {{ token('before_kyc') == 1 ? 'checked' : '' }} id="kyc-before-buy">
                                                    <label for="kyc-before-buy">Enable</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <label class="input-item-label">Purchase With</label>
                                            <ul class="d-flex flex-wrap checkbox-list checkbox-list-c5">
                                                @foreach($countries as $country)
                                                    <li>
                                                        <div class="input-item text-left">
                                                            <div class="input-wrap">
                                                                <input class="input-checkbox input-checkbox-sm all_methods"
                                                                       value="{{ $country }}"
                                                                       name="countries[]"
                                                                       {{ in_array($country, $eligibleCountries)? 'checked ' : '' }}
                                                                       id="pw-{{ $country }}"
                                                                       type="checkbox">
                                                                <label for="pw-{{ $country }}">{{ $country }}</label>
                                                            </div>
                                                        </div>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="gaps-2x"></div>
                                    <h5 class="card-title-sm text-secondary">Progress Bar Setting</h5>
                                    <div class="row">
                                        <div class="col-lg-3 col-sm-6">
                                            <div class="input-item input-with-label">
                                                <label class="input-item-label">Average Distribution Show in</label>
                                                <div class="input-wrap">
                                                    <select class="select select-block select-bordered"
                                                            name="token_sales_raised">
                                                        <option {{ token('sales_raised') == 'token' ? 'selected ' : '' }}value="token">
                                                            Token Amount
                                                        </option>
                                                        @foreach($pm_gateways as $pmg => $pmval)
                                                            @if(get_setting('pmc_active_'.$pmg) == 1)
                                                                <option {{ token('sales_raised') == $pmg ? 'selected ' : '' }}value="{{ $pmg }}">{{ $pmval.(($pmg==base_currency()) ? ' (Based)' : '') }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-6">
                                            <div class="input-item input-with-label">
                                                <label class="input-item-label">Total Amount Show in</label>
                                                <div class="input-wrap">
                                                    <select class="select select-block select-bordered"
                                                            name="token_sales_total">
                                                        <option {{ token('sales_total') == 'token' ? 'selected ' : '' }}value="token">
                                                            Token Amount
                                                        </option>
                                                        @foreach($pm_gateways as $pmg => $pmval)
                                                            @if(get_setting('pmc_active_'.$pmg) == 1)
                                                                <option {{ token('sales_total') == $pmg ? 'selected ' : '' }}value="{{ $pmg }}">{{ $pmval.(($pmg==base_currency()) ? ' (Based)' : '') }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-sm-6">
                                            <div class="input-item input-with-label">
                                                <label class="input-item-label">Users Participating Show in</label>
                                                <div class="input-wrap">
                                                    <select class="select select-block select-bordered"
                                                            name="token_sales_cap">
                                                        <option {{ token('sales_cap') == 'token' ? 'selected ' : '' }}value="token">
                                                            Token Amount
                                                        </option>
                                                        @foreach($pm_gateways as $pmg => $pmval)
                                                            @if(get_setting('pmc_active_'.$pmg) == 1)
                                                                <option {{ token('sales_cap') == $pmg ? 'selected ' : '' }}value="{{ $pmg }}">{{ $pmval.(($pmg==base_currency()) ? ' (Based)' : '') }}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="gaps-1x"></div>
                                    <div class="d-flex">
                                        <button class="btn btn-primary save-disabled" type="submit" disabled>
                                            <span><i class="ti ti-reload"></i><span>Update</span></span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <div class="sap sap-gap"></div>
                            <div class="card-text ico-setting setting-ico-userpanel">
                                <h4 class="card-title-md text-primary">Program Settings</h4>
                                <p>Manage the Global Equity Program Settings.</p>
                                <div class="gaps-1x"></div>
                                <form action="{{ route('admin.ajax.stages.settings.update') }}" method="POST"
                                      id="upanel_setting_form" class="validate-modern">
                                    @csrf
                                    <input type="hidden" name="req_type" value="program_settings">
                                    <div class="row">
                                        <div class="col-lg-3 col-sm-6">
                                            <div class="input-item input-with-label">
                                                <label class="input-item-label">Program Live</label>
                                                <div class="input-wrap input-wrap-switch">
                                                    <input class="input-switch" name="user_sales_progress"
                                                           type="checkbox"
                                                           {{ gws('user_sales_progress', 1) == 1 ? 'checked' : '' }} id="sales-progress-hide">
                                                    <label for="sales-progress-hide">Active</label>
                                                </div>
                                                <span class="input-note">Enable/Disable Global Equity program.</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="gaps-1x"></div>
                                    <div class="d-flex">
                                        <button class="btn btn-primary save-disabled" type="submit" disabled>
                                            <span><i class="ti ti-reload"></i><span>Update</span></span>
                                        </button>
                                    </div>
                                </form>
                            </div>
                            {{-- @dd($modules) --}}
                            @if(isset($modules) && !empty($modules))
                                @foreach($modules as $opt)
                                    @if(!empty($opt->view))
                                        <div class="sap sap-gap"></div>
                                        <div class="card-text ico-setting setting-ico-userpanel">
                                            @includeIf($opt->view, $opt->variables)
                                        </div>
                                    @endif
                                @endforeach
                            @endif
                        </div>{{-- .card-innr --}}
                    </div>{{-- .card --}}

                </div>{{-- .col --}}
            </div>{{-- .container --}}
        </div>{{-- .container --}}
    </div>{{-- .page-content --}}
@endsection
