@extends('layouts.admin')
@section('title', 'Bounty Stage')
<style>

</style>
@section('content')
<div class="page-content">
    <div class="container">
        <div class="card content-area">
            <div class="card-innr">
                <div class="card-head d-flex justify-content-between align-items-center">
                    <h4 class="card-title mb-0">Stage Update</h4>
                    <div class="d-flex guttar-15px">
                        <div class="fake-class">
                            <span class="badge badge-lg badge-dim badge-lighter d-none d-md-inline-block">Distributed: {{ number_format($social->soldout, 2).' '. token_symbol() }}</span>
                        </div>
                        <div class="fake-class">
                            <form action="{{ route('admin.ajax.bounty.stages.active') }}" method="POST">
                                @csrf
                                <a href="javascript:void(0);" id="update_stage" data-type = "active_stage" data-id="{{$social->id}}" class="btn btn-icon btn-sm btn-{{(get_setting('actived_stage') == $social->id)?'danger disabled':'danger-alt'}}"><em class="fas fa-star"></em></a>
                                <input class="input-bordered" type="hidden" name="actived_stage" value="{{ $social->id }}">
                            </form>
                        </div>
                        <div class="fake-class">
                            <a href="{{route('admin.bounty.stages')}}" class="btn btn-sm btn-auto btn-primary d-sm-inline-block d-none">
                                <span><em class="fas fa-arrow-left"></em><span>Back</span></span>
                            </a>
                            <a href="{{route('admin.bounty.stages')}}" class="btn btn-icon btn-sm btn-primary d-sm-none">
                                <span><em class="fas fa-arrow-left"></em></span>
                            </a>
                        </div>
                    </div>
                </div>

                <div class="gaps-1x"></div>

                <div class="tab-content" id="ico-details">
                    <div class="tab-pane fade show active" id="ico_stage">
                        <form action="{{ route('admin.ajax.bounty.update.stage') }}" class="_reload validate-modern" method="POST" id="ico_stage" autocomplete="off">
                            @csrf
                            <input type="hidden" name="social_id" value="{{ $social->id }}">
                            <div id="stageDetails" class="wide-max-md">
                                <div class="input-item input-with-label">
                                    <label class="input-item-label">{{$social->name}}
                                        @if((date('Y-m-d H:i:s') >= $social->start_date) && (date('Y-m-d H:i:s') <= $social->end_date) && ($social->status != 0))
                                        <span class="ucap badge badge-success ml-2">Running</span>
                                        @elseif((date('Y-m-d H:i:s') >= $social->start_date && date('Y-m-d H:i:s') <= $social->end_date) && ($social->status == 0))
                                        <a class="btn btn-sm btn-auto btn-primary badge btn-paused">
                                            <span class="d-none d-sm-inline-block">Paused</span>
                                        </a>
                                        @else
                                        <span class="ucap badge badge-danger ml-2">Expired</span>
                                        @endif
                                    </label>
                                    <div class="input-wrap">
                                        <input class="input-bordered" type="text" name="social_name" value="{{ $social->name }}" required>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="input-item input-with-label">
                                            <label class="input-item-label">Total Bounty Pool</label>
                                            <div class="input-wrap">
                                                <input class="input-bordered" type="number" min="1" name="total_bounty_issue" value="{{ $social->total_bounty_issue }}" required  >
                                            </div>
                                            <span class="input-note">Define how many DCAs available for this bounty.</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-item input-with-label">
                                            <label class="input-item-label">DCA</label>
                                            <div class="input-wrap">
                                                <input class="input-bordered" type="number" min="0" name="base_dca" value="{{$social->dca}}" required>
                                            </div>
                                            <span class="input-note">Define your DCA rate.</span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-item input-with-label">
                                            <label class="input-item-label">Start Date</label>
                                            <div class="row guttar-15px align-items-center">
                                                <div class="col-7 col-sm-7">
                                                    <div class="input-wrap">
                                                        <input class="input-bordered date-picker" type="text" name="start_date" value="{{ stage_date($social->start_date) }}"  required>
                                                        <span class="input-icon input-icon-right date-picker-icon"><em class="ti ti-calendar"></em></span>
                                                    </div>
                                                </div>
                                                <div class="col-5 col-sm-5">
                                                    <div class="input-wrap">
                                                        <input class="input-bordered time-picker" type="text" name="start_time" value="{{ stage_time($social->start_date) }}" >
                                                        <span class="input-icon input-icon-right time-picker-icon"><em class="ti ti-alarm-clock"></em></span>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <span class="input-note">Start date/time for bounty. Bounty is disabled before this time.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="input-item input-with-label">
                                            <label class="input-item-label">End Date</label>
                                            <div class="row guttar-15px align-items-center">
                                                <div class="col-7 col-sm-7">
                                                    <div class="input-wrap">
                                                        <input class="input-bordered custom-date-picker" type="text" name="end_date" value="{{ stage_date($social->end_date) }}"  required>
                                                        <span class="input-icon input-icon-right date-picker-icon"><em class="ti ti-calendar"></em></span>
                                                    </div>
                                                </div>
                                                <div class="col-5 col-sm-5">
                                                    <div class="input-wrap">
                                                        <input class="input-bordered time-picker" type="text" name="end_time" value="{{ stage_time($social->end_date, 'end') }}" >
                                                        <span class="input-icon input-icon-right time-picker-icon"><em class="ti ti-alarm-clock"></em></span>
                                                    </div>
                                                </div>
                                                <div class="col-12">
                                                    <span class="input-note">Finish date/time for bounty. Bounty is disabled after this time.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <div class="input-item input-with-label">
                                            <div class="row guttar-15px align-items-center">
                                                <div class="col-12">
                                                    <div class="input-wrap">
                                                        <input type="checkbox" class="input-switch" id="sale_pause" {{($social->status != 0)?'checked ':''}}name="sale_pause">
                                                        <label for="sale_pause">Campaign Running</label>
                                                    </div>
                                                    <span class="input-note">Disable this if you want to stop bounty temporarily. Note: Bounty submitters are still able to calculate their distribution amount.</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="gaps-1-5x"></div>
                                <div class="d-flex">
                                    <button class="btn btn-primary save-disabled" type="submit" disabled="">
                                        <span>Update Stage</span>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>  {{-- .tab-pane --}}
                </div>
            </div>
        </div> {{-- card content-area --}}
    </div> {{-- container --}}
</div> {{-- page-content --}}

@endsection