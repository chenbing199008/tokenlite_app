@extends('layouts.admin')
@section('title', __('Bounty'))
@section('content')
@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/bounty.css') }}">
@endsection
@push('header')
<style>
    .input-bordered:disabled {
        background: white !important;
    }
    .col-4 {
        max-width: 16.5rem;

    }
    .card-body {
        position: relative;
    }
</style>
@endpush
<div class="page-content">
    <div class="container">
        <div class="bounty">
            <div class="row">
                <div class="justify-content-center align-items-end" style="padding-left: 17rem;">
                    <div class="bounty_header">
                        <div style="width:210px; height:210px;">
                            <a>
                                <img src="/images/fantasy_characters/planets.svg" class="space_icon"/>
                            </a>
                        </div>
                        <span class="bounty_title">{{__('Bounty')}}</span>
                    </div>
                </div>
                <div class="row" style="text-align: right; align-items: flex-end; align-content: center; padding-top: 23px;">
                    <div class="image-hover-zoom">
                        <img src="/images/fantasy_characters/477154.svg" />
                    </div>
                    <div class="image-hover-zoom">
                        <img src="/images/fantasy_characters/477187.svg"/>
                    </div>
                    <div class="image-hover-zoom">
                        <img src="/images/fantasy_characters/477179.svg"/>
                    </div>
                </div>
            </div>

            <div class="row bounty-card-group">
                @foreach ($social as $item)
                @if($item->id%3 == 1)
                    <div style="width:100%; height: 8rem; margin-bottom: 150px; ">
                    </div>
                @endif
                <a class="bounty-card col-4" href="{{ route('admin.ajax.bounty.social', ['id' => $item->id], false) }}">
                    <div class="card-body">
                        <div class="border-image">
                            @if($item->image)
                                <img src="/images/fantasy_characters/{{$item->image}}"/>
                            @endif
                        </div>
                        <div style="font-size: 15px;">
                            <h5 id="title">{{$item->name}} <br /> Campaign</h5>
                            <p>({{$item->dca}} DCA)</p>
                        </div>
                    </div>
                </a>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection