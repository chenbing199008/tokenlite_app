@extends('layouts.user')
@section('title', __('Bounty'))

@section('content')
@section('styles')
    <link rel="stylesheet" href="{{ asset('assets/css/bounty.css') }}">
@endsection

@push('header')

<style>
    .input-bordered:disabled {
        background: white !important;
    }
    .col-4 {
        max-width: 16.5rem;
    }
    .col-5 {
        display: flex;
        flex-wrap: wrap;
        align-content: start;
        justify-content: flex-end;
    }
    .card-body {
        position: relative;
    }    
    .modal-lg {
        max-width: 60%;
    }

    .card-title {
        font-size: 15px;
    }

    .card-text {
        font-size: 12px;
    }

</style>
@endpush

<div class="bounty">
    <div class="row">
        <div class="justify-content-center align-items-end" style="padding-left: 17rem;">
            <div class="bounty_header">
                <div style="width:210px; height:210px;">
                    <a href="{{ route('bounty') }}">
                        <img src="/images/fantasy_characters/planets.svg" class="space_icon"/>
                    </a>
                </div>
                <span class="bounty_title">{{__('Bounty')}}</span>
            </div>
        </div>
        <div class="row" style="text-align: right; align-items: flex-end; align-content: center; padding-top: 15px;">
            <div class="image-hover-zoom">
                <img src="/images/fantasy_characters/477154.svg" />
            </div>
            <div class="image-hover-zoom">
                <img src="/images/fantasy_characters/477187.svg" />
            </div>
            <div class="image-hover-zoom">
                <img src="/images/fantasy_characters/477179.svg" />
            </div>
        </div>
    </div>

    <div class="row bounty-card-group">
        @foreach ($social as $item)
        @if($item->id%3 == 1)
            <div style="width:100%; height: 6.5rem;">
            </div>
        @endif
        @if ($item->status == 1)
        <div class="bounty-card col-4 outer-card" data-toggle="modal" data-target="#g2fa-modal" data-title="{{$item->name}}" data-id="{{$item->id}}">
        @elseif ($item->status == 0)
        <div class="bounty-card col-4 outer-card">
        @endif
            <div class="card-body">
                @if($item->status == 0)
                    <label class="warning-message">Bounty is not available</label>
                @endif
                <div class="border-image">
                    @if($item->image)
                    <img src="/images/fantasy_characters/{{$item->image}}"/>
                    @endif
                </div>
                <div style="font-size: 15px;">
                    <h5 id="title">{{$item->name}} <br /> Campaign</h5>
                    <p>({{$item->dca}} DCA)</p>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection

@push('footer')

<div class="modal fade" id="g2fa-modal" tabindex="-1">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content">
            <a href="#" class="modal-close" data-dismiss="modal" aria-label="Close"><em class="ti ti-close"></em></a>
            <div class="popup-body">
                <div class="popup-title">
                    <a class="topbar-logo" href="http://tokenlite.local">
                        <img src="http://tokenlite.local/assets/images/Decracy-Contribution-Logo-V2.png" alt="TokenLite">
                    </a>
                <div>
                <div class="text-center">
                    <h3>Submission Page</h3>
                    <p id="social" style="font-weight: bold;"></p>
                </div>
                <div class="row" style="margin-top: 3rem; justify-content: space-evenly;">
                    <div class="col-7">
                        <div class="border-bottom border-dark">
                            <p class="font-weight-bold">Bounty requirements</p>
                            <ul class="list-group" style="font-size: 12px; margin-bottom: 30px;">

                                <li style="margin-top: 10px;" id="twitter_follower_count"> 
                                    <i class="fas fa-circle"></i> 
                                    <span>{{$twitter_rule->follower_suffix}} <span id="minimum_followers">{{$twitter_rule->followers}}</span> {{$twitter_rule->follower_prefix}}.</span>
                                </li>

                                <li style="margin-top: 10px;" id="twitter_likes_count"> 
                                    <i class="fas fa-circle"></i> 
                                    <span>{{$twitter_rule->likes_suffix}} <span id="minimum_likes">{{$twitter_rule->likes}}</span> {{$twitter_rule->likes_prefix}}.</span>
                                </li>

                                <li style="margin-top: 10px;" id="twitter_retweet_count"> 
                                    <i class="fas fa-circle"></i> 
                                    <span>{{$twitter_rule->retweets_suffix}} <span id="minimum_retweets">{{$twitter_rule->retweets}}</span> {{$twitter_rule->retweets_prefix}}.</span>
                                </li>

                                <li style="margin-top: 10px;" id="facebook_friends_count"> 
                                    <i class="fas fa-circle"></i> 
                                    <span>{{$facebook_rule->follower_suffix}} <span id="minimum_retweets">{{$facebook_rule->followers}}</span> {{$facebook_rule->follower_prefix}}.</span>
                                </li>

                                @foreach ($requirements as $item)
                                    <li style="margin-top: 10px;" id="{{$item->social_id}}" class="social_req"> <i class="fas fa-circle"></i> <span id="social_req_content">{{$item->requirement}}</span></li>
                                @endforeach
                            </ul>
                        </div>
                        <form class="validate-modern bounty_form" method="POST" action="{{ route('user.ajax.bounty.submit') }}" id="bounty_form">
                            @csrf
                            <input type="hidden" name="action_type" value="google2fa_setup">
                            <input type="hidden" name="social_id" id="social_id">
                            <p class="font-weight-bold" style="margin-top: 45px;"> Bounty Details </p>
                            
                            <div style="margin-top: 45px;" class="bounty-details">
                                <div class="col-5" style="float: left; padding: 0;">
                                    <input class="input-bordered" type="text" id="first-name" name="firstName" required="required" placeholder="{{ __('First Name') }}" minlength="3">
                                </div>
                                <div class="col-5" style="float: left; padding: 0;">
                                    <input class="input-bordered" type="text" id="last-name" name="lastName" required="required" placeholder="{{ __('Last Name') }}" minlength="3">
                                </div>
                                <div style="clear: both;"></div>
                            </div>

                            <div class="mt-4 bounty-details">
                                <div class="col-5" style="float: left; padding: 0;">
                                    <input class="input-bordered" type="text" id="social-name" name="social_username" required="required" minlength="3" onchange="onChangeSocialName();" autocapitalize="none"/>
                                    <span class="dotret_input" id="red"></span>
                                    <span class="dotblue_input" id="blue"></span>
                                </div>
                                <div class="col-5" style="float: left; padding: 0;">
                                    <input class="input-bordered" type="text" id="current-follow-count" name="current_follow_count" placeholder="{{ __('Follower Count') }}" readonly />
                                    <input class="input-bordered" type="text" id="current-friends-count" name="current_friends_count" placeholder="{{ __('Friends Count') }}" readonly />
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                            <div class="mt-4 bounty-details" id="get_telegramID">
                                <div>
                                    <em class="fas fa-info-circle"></em>
                                    <a href="#" onclick="get_telegramID()"> <span style="font-size:12px;">How to get Telegram User ID? </span></a>
                                </div>
                            </div>

                            <div class="mt-4 bounty-details" id="get_discordID">
                                <div>
                                    <em class="fas fa-info-circle"></em>
                                    <a href="#" onclick="get_discordID()"> <span style="font-size:12px;">How to get Discord User ID? </span></a>
                                </div>
                            </div>
                                

                            <div class="mt-4 bounty-details">
                                <div class="col-5" style="float: left; padding: 0;">
                                    <input class="input-bordered" type="text" id="likes-count" name="likes-count" placeholder="{{ __('Likes count') }}" disabled />
                                </div>

                                <div class="col-5" style="float: left; padding: 0;">
                                    <input class="input-bordered" type="text" id="retweet-count" name="retweet-count" placeholder="{{ __('Re-tweet count') }}" disabled />
                                </div>
                                <div style="clear: both;"></div>
                            </div>
                            <div class="col mt-5" style="display: inline-flex; padding: 0; font-size: 10px">
                                <div>
                                    <span class="dotred"></span> <span style="margin-left:5px">Username Not Following Decracy</span>
                                </div>
                                <div style="margin-left: 10px;">
                                    <span class="dotblue"></span><span style="margin-left:5px"> Username Following Decracy</span>
                                </div>
                            </div>
                            <div class="col" style="padding: 0; margin-top: 30px;">
                                <span style="font-size:12px; display: flex;">
                                    <input type="checkbox" id="accept" class="chk_adjust input-checkbox input-checkbox-md">
                                    <label for="accept">I understand that I must pass KYC to receive the distribution of my bounty balance.</label>
                                </span>
                            </div>
                            <input type="hidden" name="social_url" id="social_url" />
                            <a href="#" class="btn btn-facebook" style="margin-top:25px" id="facebok_login_btn" onclick="logInWithFacebook()"> Facebook Login </a>
                            <button type="submit" class="btn btn-primary" id="modal_submit_btn" style="margin-top:25px"><span>{{ __('Submit') }}</span></button>
                        </form>
                    </div>

                    <div class="col-5 bounty-other-card-group">
                        <h4 class="text-center" style="font-weight: bold; font-size:20px">Other Bounties</h4>
                        @foreach ($modal_social as $item)
                        <div class="bounty-card bounty-other-card social-{{$item->social_id}}" id="social-{{$item->social_id}}">
                            @if($item->status == 1)
                            <div class="card-body" onclick="smallBountycard({{$item->social_id}}, '{{$item->name}}')">
                            @else
                            <div class="card-body">
                            @endif
                                @if($item->status == 0)
                                <label class="warning-message" style="font-size: 10px;">Bounty is not available</label>
                                @endif
                                <div class="bounty-other-card-image">
                                    @if($item->image)
                                    <img src="/images/fantasy_characters/{{$item->image}}"/>
                                    @endif
                                </div>
                                <h5 class="card-title" >{{$item->name}}</h5>
                                <p class="card-text mt-1">({{$item->dca}} DCA)</p>
                            </div>
                        </div>
                        @endforeach
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('assets/js/user.bounty.js') }}"></script>
<script>
    logInWithFacebook = function() {
        FB.login(function(response) {
            if (response.authResponse) {
                alert('You are logged in successfully!');
                getAccessToken();
                // Now you can redirect the user or do an AJAX request to
                // a PHP script that grabs the signed request from the cookie.
            } else {
                alert('User cancelled login or did not fully authorize.');
            }
        }, 
        {
            scope: 'email, user_friends, user_likes, user_link'
        });
        return false;
    };

    getAccessToken = function() {
        $.get('/user/facebook/callback', function(res) {
            console.log(res);
            $("#current-friends-count").val(res.friends_count + " Friends Count");
            $("#social-name").val(res.username);
            $("#social-name").prop("readonly", true);
            $("#social_url").val(res.profile_link);

            if (res.likes_count == 1) {
              $("#blue").show();
              $("#red").hide();
              $("#likes-count").val(1 + " Likes Count");
              $("#bounty_form input[type=checkbox]").click(function() {
                if ($(this).is(":checked")) {
                  $("#modal_submit_btn").prop("disabled", false);
                } else if ($(this).is(":not(:checked)")) {
                  $("#modal_submit_btn").prop("disabled", true);
                }
              });
            } else {
              $("#blue").hide();
              $("#red").show();
              $("#modal_submit_btn").prop("disabled", true);
              $("#likes-count").val(0 + " Likes Count");
            }
        });
    }

    window.fbAsyncInit = function() {
        FB.init({
            appId: '778096002673014',
            cookie: true, // This is important, it's not enabled by default
            version: 'v2.2'
        });
    };
  
    (function(d, s, id){
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {return;}
        js = d.createElement(s); js.id = id;
        js.src = "https://connect.facebook.net/en_US/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
</script>
@endpush