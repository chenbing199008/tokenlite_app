$(function() {
    $("#updateNamebtn").hide();
    $("#cancelNamebtn").hide();
    $(".socialName").prop("readonly", true);

    $("#updateDCAbtn").hide();
    $("#cancelDCAbtn").hide();
    $("#dca").prop("readonly", true);

    var status = $("#socialStatus").val();
    
    if (status == 0) {
        $(".switch :checkbox").prop('checked', false);
    }
    if (status == 1) {
        $(".switch :checkbox").prop('checked', true);
    }
    $(".switch :checkbox").change(function() {
        var socialId = $("#socialId").val();
        if (this.checked) {
            $("#socialStatus").val(1);
        } else {
            $("#socialStatus").val(0);
        }
    });

    $(".enableEdit_btn").click(function(){
        $(".editcard").addClass("d-none");
        var index = $(".enableEdit_btn").index(this);
        $(".editcard:eq("+index+")").removeClass("d-none");
    })

    $(".updateRequirement_btn").click(function(){
        var index = $(".updateRequirement_btn").index(this);
        var newRequirement = $(".updated_requirement:eq("+index+")").val();
        $(".requirement:eq("+index+")").html(newRequirement);
        $(".editcard:eq("+index+")").addClass("d-none");
    })

    $(".cancelRequirement_btn").click(function(){
        var index = $(".cancelRequirement_btn").index(this);
        $(".editcard:eq("+index+")").addClass("d-none");
    })

    $(".delete_btn").click(function(){
        var index = $(".delete_btn").index(this);
        $("#confirm-modal").modal();
        $("#item_id").val(index);
    })

    $(".yes_btn").click(function(){
        var index = $("#item_id").val();
        $("#requirement_list").children().eq(index).remove();
        $("#confirm-modal").modal('toggle');

    })

    $(".update_rule_btn").click(function() {
        var index = $("#item_id").val();
        var rule = $("#rule").val();
        $("#follow_rule").html(rule);
        $("#follow_rule_count").val(rule);
    })


    $("#add_requirement_btn").click(function(){
        var new_req = $("#new_requirement").val();
        var html = '<li class="list-group-item mt-3">' + 
            '<span class="requirement">' + new_req + '</span>' + 
            '<div class="float-right">' + 
                '<a class="text-success enableEdit_btn" style="cursor: pointer"><span>Edit</span></a>' + 
                '<a class="text-danger ml-1" data-toggle="modal" data-target="#confirm-modal" data-title="" style="cursor: pointer">' + 
                    '<span>Delete</span>' + 
                '</a>' + 
            '</div>' + 
        '</li>' + 
        '<div class="card mt-5 d-none myCard editcard">' + 
            '<div class="card-header">' + 
                'Edit' + 
        '</div>' + 
            '<div class="card-body">' + 
            '<input type="text" class="form-control col updated_requirement" name="updated_requirement[]" value="'+new_req+ '" />' + 
                '<div class="d-flex justify-content-end mt-3">' + 
                    '<a class="btn btn-primary updateRequirement_btn"><span>OK</span></a>' + 
                    '<a class="btn btn-danger ml-1 cancelRequirement_btn"><span>Cancel</span></a>' + 
                '</div>' + 
            '</div>' + 
        '</div>';
        $("#requirement_list").append(html);
        $("#new_requirement").val("");
    });

    function editName() {
        $("#editNamebtn").hide();
        $("#updateNamebtn").show();
        $("#cancelNamebtn").show();
        $(".socialName").prop("readonly", false);
    }

    function updateName() {
        $("#editNamebtn").show();
        $("#updateNamebtn").hide();
        $("#cancelNamebtn").hide();
        $(".socialName").prop("readonly", true);
    }

    function cancelName(name) {
        $("#socialName").val(name);
        $("#editNamebtn").show();
        $("#updateNamebtn").hide();
        $("#cancelNamebtn").hide();
        $(".socialName").prop("readonly", true);
    }

    function editDCA(){
        $("#editDCAbtn").hide();
        $("#updateDCAbtn").show();
        $("#cancelDCAbtn").show();
        $("#dca").prop("readonly", false);
    }
    

    function updateDCA() {
        $("#editDCAbtn").show();
        $("#updateDCAbtn").hide();
        $("#cancelDCAbtn").hide();
        $("#dca").prop("readonly", true);
    }

    function cancelDCA(dca) {
        $("#dca").val(dca);
        $("#editDCAbtn").show();
        $("#updateDCAbtn").hide();
        $("#cancelDCAbtn").hide();
        $("#dca").prop("readonly", true);
    }

    function cancel_requirement(id) {
        var req = $("#requirement"+id).html();
        $("#updated_requirement"+id).val(req);
        $("#editcard"+id).toggleClass("d-none");
    }
    function clearImage() {
        $("#clearImage").val(1);
    }
    window.editName = editName;
    window.updateName = updateName;
    window.cancelName = cancelName;
    window.editDCA = editDCA;
    window.updateDCA = updateDCA;
    window.cancelDCA = cancelDCA;
    window.cancel_requirement = cancel_requirement;
    window.clearImage = clearImage;
});