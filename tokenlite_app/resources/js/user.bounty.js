$(function() {
  var hash = window.location.hash;
  var title = "";

  $("#blue").hide();
  $("#red").hide();

  try {
    title = hash.split("#")[1];
  } catch (e) {}

  if (title && title != "") {
    $("#g2fa-modal #social").html(title);
    // modal.find("#social-name").attr("placeholder", title + "Username");
    $("#g2fa-modal").modal("show");
  }

  $("#g2fa-modal").on("shown.bs.modal", function(e) {
    $("#modal_submit_btn").prop("disabled", true);

    var modal = $(this);
    var title = $(e.relatedTarget).data("title");
    var id = $(e.relatedTarget).data("id");
    $("#g2fa-modal #social").html(title);
    $("#g2fa-modal #social_id").val(id);

    $("#social-name").prop("readonly", false);

    $("#facebook_friends_count").hide();

    if (id == 1) {
      $("#twitter_follower_count").show();
      $("#twitter_likes_count").show();
      $("#twitter_retweet_count").show();

      $("#current-friends-count").hide();
      $("#current-follow-count").show();
      $("#retweet-count").show();
      $("#likes-count").show();
    } else if (id == 2 || id == 6 || id == 5) {
      $("#twitter_follower_count").hide();
      $("#twitter_likes_count").hide();
      $("#twitter_retweet_count").hide();

      $("#current-friends-count").hide();
      $("#current-follow-count").hide();
      $("#retweet-count").hide();
      $("#likes-count").hide();
    } else if (id == 4) {
      $("#twitter_follower_count").hide();
      $("#twitter_likes_count").hide();
      $("#twitter_retweet_count").hide();

      $("#current-friends-count").show();
      $("#likes-count").show();

      $("#current-follow-count").hide();
      $("#retweet-count").hide();
    }

    if (id == 3) {
      $("#facebok_login_btn").show();
      $("#social-name").prop("readonly", true);

      $("#twitter_follower_count").hide();
      $("#twitter_likes_count").hide();
      $("#twitter_retweet_count").hide();

      $("#facebook_friends_count").show();

      $("#current-friends-count").show();
      $("#likes-count").hide();

      $("#current-follow-count").hide();
      $("#retweet-count").hide();
      modal.find("#social-name").attr("placeholder", title + " Username");
      $("#get_telegramID").hide();
      $("#get_discordID").hide();
    } else if (id == 2) {
      $("#facebok_login_btn").hide();
      modal.find("#social-name").attr("placeholder", title + " User ID");
      $("#get_telegramID").show();
      $("#get_discordID").hide();
    } else if (id == 6) {
      $("#facebok_login_btn").hide();
      modal.find("#social-name").attr("placeholder", title + " User ID");
      $("#get_telegramID").hide();
      $("#get_discordID").show();
    } else {
      $("#facebok_login_btn").hide();
      modal.find("#social-name").attr("placeholder", title + " Username");
      $("#get_telegramID").hide();
      $("#get_discordID").hide();
    }

    var reqs = document.getElementsByClassName("social_req");

    for (var i = 0; i < reqs.length; i++) {
      var social_id = $(reqs[i]).attr("id");
      if (social_id == id) {
        $(reqs[i]).show();
      } else {
        $(reqs[i]).hide();
      }
    }

    // hide bounty-other-card if social name is same
    var className = "social-" + id;
    if ($(".bounty-other-card").hasClass(className)) {
      $("#" + className).hide();
      // $(".bounty-other-card-group").children().eq(3).show();
    } else {
      $(".bounty-other-card-group")
        .children()
        .eq(3)
        .hide();
    }
  });

  $("#g2fa-modal").on("hide.bs.modal", function() {
    $(".bounty-other-card-group")
      .children()
      .show();
    $("#first-name").val("");
    $("#last-name").val("");
    $("#social-name").val("");
    $("#current-follow-count").val("");
    $("#current-friends-count").val("");
    $("#likes-count").val("");
    $("#retweet-count").val("");
    $("#likes-count").val("");
    $("#blue").hide();
    $("#red").show();
    $("#modal_submit_btn").prop("disabled", false);
    $("#facebook_friends_count").hide();
  });

  function smallBountycard(id, name) {
    // var title=$("#other_social1").text();
    var title = name;
    console.log(name);
    // window.open('bounty#' + title, '_blank');
    window.open("bounty#" + title, "_blank");
  }

  function onChangeSocialName() {
    var social_username = $("#social-name").val();
    var social_name = $("#g2fa-modal #social").text();

    $.ajax({
      async: true,
      type: "GET",
      dataType: "json",
      crossDomain: true,
      url: "http://tokenlite.local/bounty/socialUsername",
      data: {
        username: social_username,
        social_name: social_name
      },
      success: function(data) {
        if (social_name == "Twitter") {
          var following = false;
          var follower_count = 0;
          var likes_count = 0;
          var retweet_count = 0;

          var minimum_follower_count = parseInt($("#minimum_followers").html());
          var minimum_likes_count = parseInt($("#minimum_likes").html());
          var minimum_retweets_count = parseInt($("#minimum_retweets").html());

          if (data.data) {
            following = data.data["relationship"]["source"]["following"];
          }

          follower_count = data.followers_count;
          likes_count = data.likes_count;
          retweet_count = data.retweet_count;

          if (data.screen_name) {
            var url = "https://twitter.com/" + data.screen_name;
            $("#social_url").val(url);
          }

          $("#current-follow-count").val(follower_count + " Follower Count");
          $("#likes-count").val(likes_count + " Likes Count");
          $("#retweet-count").val(retweet_count + " Retweet Count");

          $("#bounty_form input[type=checkbox]").click(function() {
            if ($(this).is(":checked")) {
              if (
                follower_count >= minimum_follower_count &&
                likes_count >= minimum_likes_count &&
                retweet_count >= minimum_retweets_count &&
                following == true
              ) {
                console.log(1);
                $("#modal_submit_btn").prop("disabled", false);
              } else {
                console.log(0);
                $("#modal_submit_btn").prop("disabled", true);
              }
            } else if ($(this).is(":not(:checked)")) {
              $("#modal_submit_btn").prop("disabled", true);
            }
          });

          if (following == true) {
            $("#blue").show();
            $("#red").hide();
          } else {
            $("#blue").hide();
            $("#red").show();
          }
        } else if (social_name == "Facebook") {
          console.log("Facebook");
          console.log(data);
          var likes_data = data["likes"]["data"];
          var friends_count = data["friends"]["summary"]["total_count"];
          var url = data["link"];

          $("#current-friends-count").val(friends_count + "Friends Count");
          $("#social-name").val(data["name"]);
          $("#social-name").prop("readonly", true);
          $("#social_url").val(url);
          console.log($("#social_url").val());

          for (var i = 0; i < likes_data.length; i++) {
            console.log(likes_data[i]);
            if (likes_data[i].name == "Decracy") {
              $("#blue").show();
              $("#red").hide();
              $("#likes-count").val(1);
              $("#bounty_form input[type=checkbox]").click(function() {
                if ($(this).is(":checked")) {
                  $("#modal_submit_btn").prop("disabled", false);
                } else if ($(this).is(":not(:checked)")) {
                  $("#modal_submit_btn").prop("disabled", true);
                }
              });
              return;
            } else {
              $("#blue").hide();
              $("#red").show();
              $("#modal_submit_btn").prop("disabled", true);
              $("#likes-count").val(0 + "Likes Count");
            }
          }
        } else if (social_name == "LinkedIn") {
          console.log("linkedin");
          window.location.href = data.data;
        } else if (social_name == "Discord") {
          var flag = data;
          if (flag == 1) {
            $("#blue").show();
            $("#red").hide();
          } else {
            $("#blue").hide();
            $("#red").show();
          }
          $("#bounty_form input[type=checkbox]").click(function() {
            if ($(this).is(":checked")) {
              if (flag == 1) {
                $("#modal_submit_btn").prop("disabled", false);
              } else {
                $("#modal_submit_btn").prop("disabled", true);
              }
            } else if ($(this).is(":not(:checked)")) {
              $("#modal_submit_btn").prop("disabled", true);
            }
          });
        } else if (social_name == "Telegram") {
          var flag = data;

          if (flag == 1) {
            $("#blue").show();
            $("#red").hide();
          } else {
            $("#blue").hide();
            $("#red").show();
          }
          $("#bounty_form input[type=checkbox]").click(function() {
            if ($(this).is(":checked")) {
              if (flag == 1) {
                $("#modal_submit_btn").prop("disabled", false);
              } else {
                $("#modal_submit_btn").prop("disabled", true);
              }
            } else if ($(this).is(":not(:checked)")) {
              $("#modal_submit_btn").prop("disabled", true);
            }
          });
        } else {
          console.log("reddit");
        }
      },
      error: function() {
        alert("error");
      }
    });
  }

  function get_telegramID() {
    window.open("https://botostore.com/c/getidsbot/");
  }
  function get_discordID() {
    window.open(
      "  https://support.discordapp.com/hc/en-us/articles/206346498-Where-can-I-find-my-User-Server-Message-ID-"
    );
  }

  window.onChangeSocialName = onChangeSocialName;
  window.smallBountycard = smallBountycard;
  window.get_telegramID = get_telegramID;
  window.get_discordID = get_discordID;
});
